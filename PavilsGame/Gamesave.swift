//
//  Gamesave.swift
//  PavilsGame
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import UIKit

class Gamesave: NSObject, NSCoding
{
    var worldType: String
    var playerPositionX: CGFloat
    var playerPositionY: CGFloat
    //var saveScreenshot: UIImage?
    //var items: [Item] = []
    //var inventoryItems: [Item] = []
    
    init(worldType: String, playerPositionX: CGFloat, playerPositionY: CGFloat)//, saveScreenshot: UIImage)//, items: [Item], inventoryItems: [Item])
    {
        self.worldType = worldType
        self.playerPositionX = playerPositionX
        self.playerPositionY = playerPositionY
        //self.saveScreenshot = saveScreenshot
        //self.items = items
        //self.inventoryItems = inventoryItems

    }
    
    required convenience init(coder aDecoder: NSCoder)
    {
        let worldType = aDecoder.decodeObject(forKey: "worldType") as! String
        let playerPositionX = aDecoder.decodeObject(forKey: "playerPositionX") as! CGFloat
        let playerPositionY = aDecoder.decodeObject(forKey: "playerPositionY") as! CGFloat
        //let saveScreenshot = aDecoder.decodeObject(forKey: "saveScreenshot") as! UIImage
        //let items = aDecoder.decodeObject(forKey: "items") as! [Item]
        //let inventoryItems = aDecoder.decodeObject(forKey: "inventoryItems") as! [Item]
        self.init(worldType: worldType, playerPositionX: playerPositionX, playerPositionY: playerPositionY)//, saveScreenshot: saveScreenshot)//, items: items, inventoryItems: inventoryItems)
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(worldType, forKey: "worldType")
        aCoder.encode(playerPositionX, forKey: "playerPositionX")
        aCoder.encode(playerPositionY, forKey: "playerPositionY")
        //aCoder.encode(saveScreenshot, forKey: "saveScreenshot")
        //aCoder.encode(items, forKey: "items")
        //aCoder.encode(inventoryItems, forKey: "inventoryItems")
    }
    
}
