//
//  House1.swift
//  PavilsGame
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation
import Darwin

var loadedGame: Gamesave? = nil
var newGame: Bool = false
var exitingHouse: Bool = false

class House1: SKScene, SKPhysicsContactDelegate
{
    let worldType = "House1"
    
    var houseCollision = SKShapeNode()
    
    //CHARACTERS INITIALIZATION
    let playerCategory: UInt32 = 0x1 << 0
    let treeCategory: UInt32 = 0x1 << 0
    
    //TOUCH POINTS
    var touch: UITouch? = nil
    var touchLocation: CGPoint? = nil
    var previousLocation: CGPoint? = nil
    var pressedControls = [SKSpriteNode: UITouch]()
    var playerDirection = "front"
    var inAttack: Bool = false
    
    //SPRITE NODES DEFINITION
    var worldA = SKSpriteNode()
    var worldB = SKSpriteNode()
    var worldC = SKSpriteNode()
    var worldD = SKSpriteNode()
    var sprintButton = SKSpriteNode()
    var actionButton = SKSpriteNode()
    var attackButton = SKSpriteNode()
    var pickUpButton = SKSpriteNode()
    
    //UI NODES
    let nameHUDLabel = SKLabelNode(fontNamed: "Georgia")
    var vitalHUD = SKSpriteNode()
    var vitalityBar = SKSpriteNode(imageNamed: "lifebar.png")
    var staminaBar = SKSpriteNode(imageNamed: "staminabar.png")
    var overviewHud = SKSpriteNode()
    var timeHud = SKLabelNode()

    let fireplace = SKSpriteNode(imageNamed: "fireplace_20000.png")
    var fireplaceIterator: Int = 0
    
    //ANALOG
    var analog = SKSpriteNode()
    var analogUp = SKShapeNode()
    var analogDown = SKShapeNode()
    var analogLeft = SKShapeNode()
    var analogRight = SKShapeNode()
    var analogDefault = SKShapeNode()
    
    //LIGHTING
    var screenScale: CGFloat = 1.0
    var screenH: CGFloat = 640.0
    var screenW: CGFloat = 960.0
    
    var lightSprite: SKSpriteNode?
    var ambientColor = UIColor.darkGray
    
    //TEXTURES
    
    //UI TEXTURES
    var analogTexture = SKTexture(imageNamed: "analog.png")
    var sprintTexture = SKTexture(imageNamed: "sprint.png")
    var actionTexture = SKTexture(imageNamed: "actionButton.png")
    var attackTexture = SKTexture(imageNamed: "attackButton.png")
    var pickUpTexture = SKTexture(imageNamed: "pickUpButton.png")
    var sprintPressedTexture = SKTexture(imageNamed: "sprintPressed.png")
    var vitalHUDTexture = SKTexture(imageNamed: "HUDparchment.png")
    
    //MENU
    var ingameMenu: IngameMenu?
    var screenshot: UIImage?
    
    //MAP
    var gameMap = SKSpriteNode(imageNamed: "gameMap.png")
    var gameMarkers: [SKNode] = []
    var gameMarker = SKShapeNode(circleOfRadius: 3)
    var playerHouseMarker = SKSpriteNode(imageNamed: "hut.png")
    var millHouseMarker = SKSpriteNode(imageNamed: "mill.png")
    var millWheelMarker = SKSpriteNode(imageNamed: "millsprite0.png")
    var townBuilding1Marker = SKSpriteNode(imageNamed: "townBuilding1.png")
    var townBuilding2Marker = SKSpriteNode(imageNamed: "townBuilding2.png")
    var townBuilding3Marker = SKSpriteNode(imageNamed: "townBuilding3.png")
    var townBuilding4Marker = SKSpriteNode(imageNamed: "townBuilding4.png")
    var townBuilding5Marker = SKSpriteNode(imageNamed: "townBuilding5.png")
    var townBuilding6Marker = SKSpriteNode(imageNamed: "townBuilding6.png")
    
    //WORLD TEXTURES
    var worldTextures: [SKTexture] = []
    
    //CAMERA
    var cam: SKCameraNode!
    
    //SWITCHES
    var toMenu: Bool = false
    var inMenu: Bool = false
    
    //ACTION AREAS
    var actionAreas: [SKShapeNode] = []
    
    var buildingOverview = SKSpriteNode()
    
    override func didMove(to view: SKView)
    {
        self.backgroundColor = UIColor.darkGray
        
        ingameMenu = IngameMenu(selfViewWidth: (self.view?.frame.width)!, selfViewHeight: (self.view?.frame.height)!)
        self.physicsWorld.contactDelegate = self
        
        houseCollision = SKShapeNode(rect: CGRect(x: 20, y: 0, width: 40, height: 5))
        houseCollision.zPosition = 2
        houseCollision.alpha = 0.5
        houseCollision.fillColor = SKColor.blue
        houseCollision.position = CGPoint(x: 605.69, y: 171.314)
        self.addChild(houseCollision)
        
        actionAreas.append(houseCollision)
        
        //LIGHTING
        screenH = view.frame.height
        screenW = view.frame.width
        screenScale = screenW / 1920
        
        ambientColor = UIColor.darkGray
        lightSprite = SKSpriteNode()
        lightSprite?.setScale(screenScale * 2)
        lightSprite?.position = CGPoint(x: screenW - 100, y: screenH - 100)
        self.addChild(lightSprite!)
        
        let light = SKLightNode()
        light.position = CGPoint(x: 0, y: 0)
        light.falloff = 1
        light.ambientColor = ambientColor
        light.lightColor = UIColor.white
        //pavils.hero.addChild(light)
        
        //pavils.hero.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        pavils.hero.position = CGPoint(x: (loadedGame?.playerPositionX)!, y: (loadedGame?.playerPositionY)!)
        pavils.hero.zPosition = 2
        pavils.hero.physicsBody = SKPhysicsBody(rectangleOf: pavils.hero.size)
        pavils.hero.physicsBody?.affectedByGravity = false
        pavils.hero.physicsBody?.isDynamic = true
        pavils.hero.physicsBody?.allowsRotation = false
        pavils.hero.physicsBody?.usesPreciseCollisionDetection = true
        pavils.hero.physicsBody?.categoryBitMask = playerCategory
        pavils.hero.lightingBitMask = 1
        
        
        if (pavils.hero.parent != nil)
        {
            pavils.hero.removeFromParent()
        }
        self.addChild(pavils.hero)
        
        pavils.hero.position = CGPoint(x: 587.39, y: 169.366)
        
        //SPRITE NODE TEXTURES IMPLEMENTATION
        let hutbackgroundupTexture = SKTexture(imageNamed: "hutbackgroundup.png")
        let hutbackgrounddownTexture = SKTexture(imageNamed: "hutbackgroundown.png")
        let hutgroundTexture = SKTexture(imageNamed: "hutground.png")
        
        let hutbackgroundup = SKSpriteNode(texture: hutbackgroundupTexture)
        hutbackgroundup.lightingBitMask = 1
        hutbackgroundup.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        hutbackgroundup.physicsBody = SKPhysicsBody(texture: hutbackgroundupTexture, size: hutbackgroundupTexture.size())
        hutbackgroundup.physicsBody?.usesPreciseCollisionDetection = true
        hutbackgroundup.physicsBody?.affectedByGravity = false
        hutbackgroundup.physicsBody?.isDynamic = false
        hutbackgroundup.physicsBody?.categoryBitMask = treeCategory
        hutbackgroundup.physicsBody?.allowsRotation = false
        self.addChild(hutbackgroundup)
        
        let hutbackgrounddown = SKSpriteNode(texture: hutbackgrounddownTexture)
        hutbackgrounddown.lightingBitMask = 1
        hutbackgrounddown.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 - hutbackgroundup.frame.height + 1)
        hutbackgrounddown.physicsBody = SKPhysicsBody(texture: hutbackgrounddownTexture, size: hutbackgrounddownTexture.size())
        hutbackgrounddown.physicsBody?.usesPreciseCollisionDetection = true
        hutbackgrounddown.physicsBody?.affectedByGravity = false
        hutbackgrounddown.physicsBody?.isDynamic = false
        hutbackgrounddown.physicsBody?.categoryBitMask = treeCategory
        hutbackgrounddown.physicsBody?.allowsRotation = false
        self.addChild(hutbackgrounddown)
        
        //STRUCTURES
        let bedTexture = SKTexture(imageNamed: "bed0002.png")
        let bed = SKSpriteNode(texture: bedTexture)
        bed.lightingBitMask = 1
        bed.position = CGPoint(x: (self.view?.frame.midX)!, y: ((self.view?.frame.midY)! - 130))
        bed.zPosition = 2
        bed.physicsBody = SKPhysicsBody(texture: bedTexture, size: bedTexture.size())
        bed.physicsBody?.usesPreciseCollisionDetection = true
        bed.physicsBody?.affectedByGravity = false
        bed.physicsBody?.isDynamic = false
        bed.physicsBody?.categoryBitMask = treeCategory
        bed.physicsBody?.allowsRotation = false
        self.addChild(bed)
        
        
        fireplace.lightingBitMask = 1
        fireplace.position = CGPoint(x: 321.31, y: 149.354)
        fireplace.zPosition = 2
        fireplace.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "fireplace_20000.png"), size: SKTexture(imageNamed: "fireplace_20000.png").size())
        fireplace.physicsBody?.usesPreciseCollisionDetection = true
        fireplace.physicsBody?.affectedByGravity = false
        fireplace.physicsBody?.isDynamic = false
        fireplace.physicsBody?.categoryBitMask = treeCategory
        fireplace.physicsBody?.allowsRotation = false
        self.addChild(fireplace)
        
        //UI RENDERING
        cam = SKCameraNode()
        cam.xScale = 1.0
        cam.yScale = 1.0
        self.camera = cam
        self.addChild(cam)
        cam.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        
        overviewHud = SKSpriteNode(texture: vitalHUDTexture)
        overviewHud.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.size.height - 50), to: cam)
        overviewHud.zPosition = 6
        cam.addChild(overviewHud)
        
        buildingOverview = SKSpriteNode(texture: SKTexture(imageNamed: "hut.png"))
        buildingOverview.size = CGSize(width: 90, height: 60)
        buildingOverview.zPosition = 7
        buildingOverview.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.frame.height - 50), to: cam)
        cam.addChild(buildingOverview)
        
        let analogHeight: CGFloat = 50.0
        analogDefault = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogDefault.fillColor = UIColor.red
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            analogDefault.position = convert(CGPoint(x: self.frame.width / 7 - analogHeight / 2, y: self.frame.midY / 2 - analogHeight / 2), to: cam)
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            analogDefault.position = convert(CGPoint(x: self.frame.width / 9 - analogHeight / 2, y: self.frame.midY / 3 - analogHeight / 2), to: cam)
        }
        analogDefault.zPosition = 4
        cam.addChild(analogDefault)
        
        //ANALOG RENDERING
        analog = SKSpriteNode(texture: analogTexture)
        analog.position = CGPoint(x: analogDefault.position.x + analogHeight / 2, y: analogDefault.position.y + analogHeight / 2)
        analog.zPosition = 4
        cam.addChild(analog)
        
        let analogPosition = CGPoint(x: analog.position.x, y: analog.position.y)
        
        analogUp = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogUp.fillColor = UIColor.green
        analogUp.position = (CGPoint(x: analogPosition.x - analogHeight / 2, y: analogPosition.y + analogHeight / 2 + 10))
        analogUp.zPosition = 4
        cam.addChild(analogUp)
        
        analogDown = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogDown.fillColor = UIColor.green
        analogDown.position = (CGPoint(x: analogPosition.x - analogHeight / 2, y: analogPosition.y - analogHeight * 1.5 - 10))
        analogDown.zPosition = 4
        cam.addChild(analogDown)
        
        analogLeft = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogLeft.fillColor = UIColor.green
        analogLeft.position = (CGPoint(x: analogPosition.x - analogHeight * 1.5 - 10, y: analogPosition.y - analogHeight / 2))
        analogLeft.zPosition = 4
        cam.addChild(analogLeft)
        
        analogRight = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogRight.fillColor = UIColor.green
        analogRight.position = (CGPoint(x: analogPosition.x + analogHeight / 2 + 10, y: analogPosition.y - analogHeight / 2))
        analogRight.zPosition = 4
        cam.addChild(analogRight)
        
        sprintButton = SKSpriteNode(texture: sprintTexture)
        sprintButton.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.frame.midY / 3), to: cam)
        sprintButton.zPosition = 7
        cam.addChild(sprintButton)
        
        actionButton = SKSpriteNode(texture: actionTexture)
        actionButton.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.frame.midY / 1.35), to: cam)
        actionButton.zPosition = 7
        actionButton.isHidden = true
        cam.addChild(actionButton)
        
        attackButton = SKSpriteNode(texture: attackTexture)
        attackButton.position = convert(CGPoint(x: self.frame.width / 1.3, y: self.frame.midY / 3), to: cam)
        attackButton.zPosition = 7
        cam.addChild(attackButton)
        
        pickUpButton = SKSpriteNode(texture: pickUpTexture)
        pickUpButton.position = convert(CGPoint(x: self.frame.width / 1.3, y: self.frame.midY / 1.35), to: cam)
        pickUpButton.zPosition = 7
        cam.addChild(pickUpButton)
        pickUpButton.isHidden = true
        
        vitalHUD = SKSpriteNode(texture: vitalHUDTexture)
        vitalHUD.position = convert(CGPoint(x: 70, y: self.size.height - 50), to: cam)
        vitalHUD.zPosition = 4
        cam.addChild(vitalHUD)
        
        nameHUDLabel.text = "Pavils"
        nameHUDLabel.fontSize = 20
        nameHUDLabel.zPosition = 8
        nameHUDLabel.fontColor = UIColor.black
        nameHUDLabel.position = convert(CGPoint(x: 45, y: self.size.height - 35), to: cam)
        cam.addChild(nameHUDLabel)
        
        vitalityBar.size = CGSize(width: pavils.vitality, height: 10.0)
        vitalityBar.position = convert(CGPoint(x: 70, y: self.size.height - 52), to: cam)
        vitalityBar.zPosition = 8
        cam.addChild(vitalityBar)
        
        staminaBar.size = CGSize(width: pavils.stamina, height: 10.0)
        staminaBar.position = convert(CGPoint(x: 70, y: self.size.height - 70), to: cam)
        staminaBar.zPosition = 8
        cam.addChild(staminaBar)
        
        //MENU
        
        if UIDevice.current.userInterfaceIdiom == .phone || UIDevice.current.userInterfaceIdiom == .pad
        {
            ingameMenu?.menuBorder.position = convert(CGPoint(x: 100, y: 40), to: cam)
            ingameMenu?.menuButton1.position = convert(CGPoint(x: 100, y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            ingameMenu?.menuButton2.position = convert(CGPoint(x: (100 + ((ingameMenu?.selfViewWidth)! - 220) / 3), y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            ingameMenu?.menuButton3.position = convert(CGPoint(x: (100 + (2 * ((ingameMenu?.selfViewWidth)! - 220) / 3)), y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            
            ingameMenu?.exitButtonX.position = convert(CGPoint(x: (100 + (2.7 * ((ingameMenu?.selfViewWidth)! - 100) / 3)), y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            
            ingameMenu?.menuButton1Text.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! + 10)
            ingameMenu?.menuButton2Text.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + ((ingameMenu?.menuButton2.frame.width)! / 2), y: (ingameMenu?.menuButton2.position.y)! + 10)
            ingameMenu?.menuButton3Text.position = CGPoint(x: (ingameMenu?.menuButton3.position.x)! + ((ingameMenu?.menuButton3.frame.width)! / 2), y: (ingameMenu?.menuButton3.position.y)! + 10)
            
            ingameMenu?.gameMenuSaveButtonText.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! - 50)
            ingameMenu?.gameMenuLoadButtonText.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! - 150)
            ingameMenu?.gameMenuExitButtonText.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! - 250)
            
            ingameMenu?.savePosition1.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 85)
            ingameMenu?.savePosition2.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 175)
            ingameMenu?.savePosition3.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 265)
            
            ingameMenu?.savePosition1Image.position = CGPoint(x: (ingameMenu?.savePosition1.position.x)! + (ingameMenu?.savePosition1.frame.width)! / 2, y: (ingameMenu?.savePosition1.position.y)! + ((ingameMenu?.savePosition1.frame.height)! / 2))
            ingameMenu?.savePosition2Image.position = CGPoint(x: (ingameMenu?.savePosition2.position.x)! + (ingameMenu?.savePosition2.frame.width)! / 2, y: (ingameMenu?.savePosition2.position.y)! + ((ingameMenu?.savePosition2.frame.height)! / 2))
            ingameMenu?.savePosition3Image.position = CGPoint(x: (ingameMenu?.savePosition3.position.x)! + (ingameMenu?.savePosition3.frame.width)! / 2, y: (ingameMenu?.savePosition3.position.y)! + ((ingameMenu?.savePosition3.frame.height)! / 2))
            
            ingameMenu?.loadPosition1.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 85)
            ingameMenu?.loadPosition2.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 175)
            ingameMenu?.loadPosition3.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 265)
            
            ingameMenu?.loadPosition1Image.position = CGPoint(x: (ingameMenu?.loadPosition1.position.x)! + (ingameMenu?.loadPosition1.frame.width)! / 2, y: (ingameMenu?.loadPosition1.position.y)! + ((ingameMenu?.loadPosition1.frame.height)! / 2))
            ingameMenu?.loadPosition2Image.position = CGPoint(x: (ingameMenu?.loadPosition2.position.x)! + (ingameMenu?.loadPosition2.frame.width)! / 2, y: (ingameMenu?.loadPosition2.position.y)! + ((ingameMenu?.loadPosition2.frame.height)! / 2))
            ingameMenu?.loadPosition3Image.position = CGPoint(x: (ingameMenu?.loadPosition3.position.x)! + (ingameMenu?.loadPosition3.frame.width)! / 2, y: (ingameMenu?.loadPosition3.position.y)! + ((ingameMenu?.loadPosition3.frame.height)! / 2))
            
            ingameMenu?.pavilsName.position = CGPoint(x: (ingameMenu?.menuButton1Text.position.x)!, y: (ingameMenu?.menuButton1Text.position.y)! - 100)
            ingameMenu?.pavilsCharacter.position = CGPoint(x: (ingameMenu?.pavilsName.position.x)!, y: (ingameMenu?.pavilsName.position.y)! - 100)
            
            ingameMenu?.topAttireName.position = CGPoint(x: (ingameMenu?.menuButton2Text.position.x)!, y: (ingameMenu?.menuButton2Text.position.y)! - 100)
            ingameMenu?.topAttireSlot.position = CGPoint(x: (ingameMenu?.topAttireName.position.x)! - ((ingameMenu?.topAttireName.frame.width)! / 2), y: (ingameMenu?.topAttireName.position.y)! - 100)
            
            ingameMenu?.bottomAttireName.position = CGPoint(x: (ingameMenu?.menuButton3Text.position.x)!, y: (ingameMenu?.menuButton3Text.position.y)! - 100)
            ingameMenu?.bottomAttireSlot.position = CGPoint(x: (ingameMenu?.bottomAttireName.position.x)! - ((ingameMenu?.bottomAttireName.frame.width)! / 2.5), y: (ingameMenu?.bottomAttireName.position.y)! - 100)
            
            ingameMenu?.pavilsArmor.position = CGPoint(x: (ingameMenu?.topAttireSlot.position.x)! + (ingameMenu?.topAttireSlot.frame.width)! / 2, y: (ingameMenu?.topAttireSlot.position.y)! + (ingameMenu?.topAttireSlot.frame.height)! / 2)
            
            ingameMenu?.pavilsGraves.position = CGPoint(x: (ingameMenu?.bottomAttireSlot.position.x)! + (ingameMenu?.bottomAttireSlot.frame.width)! / 2, y: (ingameMenu?.bottomAttireSlot.position.y)! + (ingameMenu?.bottomAttireSlot.frame.height)! / 2)
            
            ingameMenu?.itemsName.position = CGPoint(x: (ingameMenu?.menuButton2Text.position.x)!, y: (ingameMenu?.topAttireSlot.position.y)! - 150)
            
            for i in 0..<Int((ingameMenu?.itemSlots.count)!)
            {
                ingameMenu?.itemSlots[i].position = CGPoint(x: ((ingameMenu?.pavilsName.position.x)! + (CGFloat(i) * 100.0)), y: (ingameMenu?.topAttireSlot.position.y)! - 300)
                if i >= pavils.numberOfItemSlots
                {
                    ingameMenu?.itemSlots[i].strokeColor = UIColor.gray
                    ingameMenu?.itemSlots[i].fillColor = UIColor.gray
                }
            }
            
            questHandler.gameQuestLabel.position = CGPoint(x: vitalityBar.position.x + 420, y: vitalityBar.position.y)
            cam.addChild(questHandler.gameQuestLabel)
        
            
        }

        
        for component in (ingameMenu?.menuComponents)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.menu2Components)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.menu3Components)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.saveComponents)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.loadComponents)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        
        /*//BLUR
         let duration: CGFloat = 0.5
         let filter: CIFilter = CIFilter(name: "CIGaussianBlur", withInputParameters: ["inputRadius" : NSNumber(value:1.0)])!
         scene!.filter = filter
         scene!.shouldRasterize = true
         scene!.shouldEnableEffects = true
         scene!.run(SKAction.customAction(withDuration: 0.5, actionBlock: { (node: SKNode, elapsedTime: CGFloat) in
         let radius = (elapsedTime/duration)*10.0
         (node as? SKEffectNode)!.filter!.setValue(radius, forKey: "inputRadius")
         
         }))*/
        
        //if newGame == true
        //{
        
        
        //}
        
        
        
    }
    
    
    //MENU
    func renderMenu()
    {
        
    }
    
    //TOUCH IMPLEMENTATION
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if newGame == false
        {
            for touch in touches
            {
                
                
                let touchLocation = touch.location(in: cam)
                if actionButton.contains(touchLocation) && inMenu == false
                {
                    if pavils.hero.intersects(houseCollision)
                    {
                        exitingHouse = true
                        questHandler.gameQuestLabel.removeFromParent()
                        pavils.hero.removeFromParent()
                        if let view = view
                        {
                            let transition:SKTransition = SKTransition.fade(withDuration: 2)
                            let scene: SKScene = GameScene(size: view.bounds.size)
                            self.view?.presentScene(scene, transition: transition)
                        }
                    }
                }
                
                
                
                if analog.contains(touchLocation) && inMenu == false
                {
                    print("ANALOG PRESSED")
                    pressedControls[analog] = touch as UITouch
                }
                if attackButton.contains(touchLocation) && inMenu == false
                {
                    print("ATTACK PRESSED")
                    inAttack = true
                }
                if sprintButton.contains(touchLocation) && inMenu == false
                {
                    pressedControls[sprintButton] = touch as UITouch
                    sprintButton.texture = sprintPressedTexture
                }
                if inMenu == false && pickUpButton.contains(touchLocation)
                {
                    for i in 0..<pavils.items.count
                    {
                        if pavils.hero.intersects(pavils.items[i].itemObject)
                        {
                            if pavils.numberOfItems < pavils.numberOfItemSlots
                            {
                                let invItem = pavils.items[i]
                                invItem.itemObject.size = CGSize(width: 80, height: 80)
                                invItem.itemObject.zPosition = 14
                                ingameMenu?.items.append(invItem)
                                pavils.numberOfItems += 1
                                pavils.items[i].itemObject.removeFromParent()
                                SKTAudio.sharedInstance().playSoundEffect("swordSwing.mp3")
                                pavils.items.remove(at: i)
                                break
                            }
                            else
                            {
                                let noSpaceMessage = UIAlertController(title: "Pavils has no space for this item!", message: "Throw away some previous items!", preferredStyle: .alert)
                                noSpaceMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.view?.window?.rootViewController?.present(noSpaceMessage, animated: true, completion: nil)
                            }
                        }
                    }
                    
                    for i in 0..<Int((ingameMenu?.items.count)!)
                    {
                        ingameMenu?.items[i].itemObject.position = CGPoint(x: (ingameMenu?.itemSlots[i].position.x)! + (ingameMenu?.itemSlots[i].frame.width)! / 2, y: (ingameMenu?.itemSlots[i].position.y)! + (ingameMenu?.itemSlots[i].frame.height)! / 2)
                        
                        ingameMenu?.items[i].itemObject.isHidden = false
                        if ingameMenu?.items[i].itemObject.parent != cam
                        {
                            cam.addChild((ingameMenu?.items[i].itemObject)!)
                        }
                        
                        ingameMenu?.items[i].itemObject.isHidden = true
                    }
                }
                
                
                if vitalHUD.contains(touchLocation) || vitalityBar.contains(touchLocation) || staminaBar.contains(touchLocation)
                {
                    self.screenshot = captureScreen()
                    SKTAudio.sharedInstance().playSoundEffect("pageTurn.wav")
                    toMenu = true
                    for marker in gameMarkers
                    {
                        marker.isHidden = true
                    }
                    if ingameMenu?.menuButton1Text.fontColor == UIColor.black
                    {
                        ingameMenu?.showMenu1Components()
                    }
                    else if ingameMenu?.menuButton2Text.fontColor == UIColor.black
                    {
                        ingameMenu?.showMenu2Components()
                    }
                    else if ingameMenu?.menuButton3Text.fontColor == UIColor.black
                    {
                        ingameMenu?.showMenu3Components()
                    }
                }
                
                for i in 0..<Int((ingameMenu?.items.count)!)
                {
                    if inMenu == true && ingameMenu?.items[i].itemObject.isHidden == false && (ingameMenu?.items[i].itemObject.contains(touchLocation))!
                    {
                        let droppedItem = ingameMenu?.items[i]
                        ingameMenu?.items[i].itemObject.removeFromParent()
                        droppedItem?.itemObject.zPosition = 1
                        droppedItem?.itemObject.size = CGSize(width: 30, height: 30)
                        droppedItem?.itemObject.position = pavils.hero.position
                        self.addChild((droppedItem?.itemObject)!)
                        ingameMenu?.items.remove(at: i)
                        pavils.numberOfItems -= 1
                        pavils.items.append(droppedItem!)
                        SKTAudio.sharedInstanceEffects().playSoundEffect("swordSwing.mp3")
                        break
                    }
                }
                
                if (ingameMenu?.exitButtonX.contains(touchLocation))! && inMenu == true
                {
                    ingameMenu?.hideAllMenuComponents()
                    ingameMenu?.hidePlayerItems()
                    SKTAudio.sharedInstance().playSoundEffect("pageTurn.wav")
                    inMenu = false
                    for marker in gameMarkers
                    {
                        marker.isHidden = false
                    }
                }
                if inMenu == true && ((ingameMenu?.menuButton1Text.contains(touchLocation))! || (ingameMenu?.menuButton1.contains(touchLocation))!)
                {
                    ingameMenu?.showMenu1Components()
                    ingameMenu?.hideMenu2Subcomponents()
                    ingameMenu?.hidePlayerItems()
                    ingameMenu?.menuButton1Text.fontColor = UIColor.black
                    ingameMenu?.menuButton2Text.fontColor = UIColor.white
                    ingameMenu?.menuButton3Text.fontColor = UIColor.white
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                }
                if inMenu == true && ((ingameMenu?.menuButton2Text.contains(touchLocation))! || (ingameMenu?.menuButton2.contains(touchLocation))!)
                {
                    ingameMenu?.showMenu2Components()
                    ingameMenu?.showPlayerItems()
                    ingameMenu?.menuButton1Text.fontColor = UIColor.white
                    ingameMenu?.menuButton2Text.fontColor = UIColor.black
                    ingameMenu?.menuButton3Text.fontColor = UIColor.white
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                }
                
                if inMenu == true && ((ingameMenu?.menuButton3Text.contains(touchLocation))! || (ingameMenu?.menuButton3.contains(touchLocation))!)
                {
                    ingameMenu?.hideMenu2Subcomponents()
                    ingameMenu?.showMenu3Components()
                    ingameMenu?.hidePlayerItems()
                    ingameMenu?.menuButton1Text.fontColor = UIColor.white
                    ingameMenu?.menuButton2Text.fontColor = UIColor.white
                    ingameMenu?.menuButton3Text.fontColor = UIColor.black
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                }
                
                if (ingameMenu?.gameMenuSaveButtonText.contains(touchLocation))!
                {
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.black
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    ingameMenu?.showSaveSubmenuComponents()
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                    ingameMenu?.savingGame = true
                    ingameMenu?.loadingGame = false
                }
                if (ingameMenu?.gameMenuLoadButtonText.contains(touchLocation))!
                {
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.black
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    ingameMenu?.showLoadSubmenuComponents()
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                    ingameMenu?.savingGame = false
                    ingameMenu?.loadingGame = true
                }
                if (ingameMenu?.gameMenuExitButtonText.contains(touchLocation))!
                {
                    exit(0)
                }
                
                if inMenu == true && ingameMenu?.savingGame == true
                {
                    if screenshot == nil
                    {
                        screenshot = captureScreen()
                    }
                    
                    let userDefaults = UserDefaults.standard
                    if let data = userDefaults.object(forKey: "savePosition1") as? Data
                    {
                        let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame1")!)
                        ingameMenu?.savePosition1Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition2") as? Data
                    {
                        let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame2")!)
                        ingameMenu?.savePosition2Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition3") as? Data
                    {
                        let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame3")!)
                        ingameMenu?.savePosition3Image.texture = saveGameTexture
                    }
                    
                    
                    if (ingameMenu?.savePosition1.contains(touchLocation))!
                    {
                        SKTAudio.sharedInstance().playSoundEffect("click.wav")
                        let gamesave = Gamesave(worldType: self.worldType, playerPositionX: pavils.hero.position.x, playerPositionY: pavils.hero.position.y)//, saveScreenshot: screenshot!)//, items: pavils.items, inventoryItems: (ingameMenu?.items)!)
                        if let screen = screenshot?.scaleImage(toSize: CGSize(width: (ingameMenu?.savePosition1Image.frame.width)!, height: (ingameMenu?.savePosition1Image.frame.height)!))
                        {
                            screenshot = screen
                        }
                        
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: gamesave)
                        userDefaults.set(encodedData, forKey: "savePosition1")
                        userDefaults.synchronize()
                        print("SAVED 1")
                        
                        let saveGameTexture = SKTexture(image: screenshot!)
                        ingameMenu?.savePosition1Image.texture = saveGameTexture
                    }
                    if (ingameMenu?.savePosition2.contains(touchLocation))!
                    {
                        SKTAudio.sharedInstance().playSoundEffect("click.wav")
                        let gamesave = Gamesave(worldType: self.worldType, playerPositionX: pavils.hero.position.x, playerPositionY: pavils.hero.position.y)//, saveScreenshot: screenshot!)//, items: pavils.items, inventoryItems: (ingameMenu?.items)!)
                        if let screen = screenshot?.scaleImage(toSize: CGSize(width: (ingameMenu?.savePosition2Image.frame.width)!, height: (ingameMenu?.savePosition2Image.frame.height)!))
                        {
                            screenshot = screen
                        }
                        
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: gamesave)
                        userDefaults.set(encodedData, forKey: "savePosition2")
                        userDefaults.synchronize()
                        print("SAVED 2")
                        
                        let saveGameTexture = SKTexture(image: screenshot!)
                        ingameMenu?.savePosition2Image.texture = saveGameTexture
                    }
                    if (ingameMenu?.savePosition3.contains(touchLocation))!
                    {
                        SKTAudio.sharedInstance().playSoundEffect("click.wav")
                        let gamesave = Gamesave(worldType: self.worldType, playerPositionX: pavils.hero.position.x, playerPositionY: pavils.hero.position.y)//, saveScreenshot: screenshot!)//, items: pavils.items, inventoryItems: (ingameMenu?.items)!)
                        if let screen = screenshot?.scaleImage(toSize: CGSize(width: (ingameMenu?.savePosition3Image.frame.width)!, height: (ingameMenu?.savePosition3Image.frame.height)!))
                        {
                            screenshot = screen
                        }
                        
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: gamesave)
                        userDefaults.set(encodedData, forKey: "savePosition3")
                        userDefaults.synchronize()
                        print("SAVED 3")
                        
                        let saveGameTexture = SKTexture(image: screenshot!)
                        ingameMenu?.savePosition3Image.texture = saveGameTexture
                        
                    }
                }
                
                if inMenu == true && ingameMenu?.loadingGame == true
                {
                    let userDefaults = UserDefaults.standard
                    if let data = userDefaults.object(forKey: "savePosition1") as? Data
                    {
                        let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame1")!)
                        ingameMenu?.loadPosition1Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition2") as? Data
                    {
                        let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame2")!)
                        ingameMenu?.loadPosition2Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition3") as? Data
                    {
                        let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame3")!)
                        ingameMenu?.loadPosition3Image.texture = saveGameTexture
                    }
                    
                    if (ingameMenu?.loadPosition1.contains(touchLocation))!
                    {
                        if let data = userDefaults.object(forKey: "savePosition1") as? Data
                        {
                            let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                            loadedGame = Gamesave(worldType: self.worldType, playerPositionX: saveGame1.playerPositionX, playerPositionY: saveGame1.playerPositionY)//, saveScreenshot: saveGame1.saveScreenshot!)//, items: saveGame1.items, inventoryItems: saveGame1.inventoryItems)
                            if let view = view
                            {
                                SKTAudio.sharedInstance().playSoundEffect("click.wav")
                                let transition:SKTransition = SKTransition.fade(withDuration: 2)
                                var scene = SKScene()
                                if loadedGame?.worldType == "GameScene"
                                {
                                    scene = GameScene(size: view.bounds.size)
                                }
                                else if loadedGame?.worldType == "House1"
                                {
                                    scene = House1(size: view.bounds.size)
                                }
                                self.view?.presentScene(scene, transition: transition)
                            }
                        }
                    }
                    if (ingameMenu?.loadPosition2.contains(touchLocation))!
                    {
                        if let data = userDefaults.object(forKey: "savePosition2") as? Data
                        {
                            let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                            loadedGame = Gamesave(worldType: self.worldType, playerPositionX: saveGame2.playerPositionX, playerPositionY: saveGame2.playerPositionY)//, saveScreenshot: saveGame2.saveScreenshot!)//, items: saveGame2.items, inventoryItems: saveGame2.inventoryItems)
                            if let view = view
                            {
                                SKTAudio.sharedInstance().playSoundEffect("click.wav")
                                let transition:SKTransition = SKTransition.fade(withDuration: 2)
                                var scene = SKScene()
                                if loadedGame?.worldType == "GameScene"
                                {
                                    scene = GameScene(size: view.bounds.size)
                                }
                                else if loadedGame?.worldType == "House1"
                                {
                                    scene = House1(size: view.bounds.size)
                                }
                                self.view?.presentScene(scene, transition: transition)
                            }
                        }
                    }
                    if (ingameMenu?.loadPosition3.contains(touchLocation))!
                    {
                        if let data = userDefaults.object(forKey: "savePosition3") as? Data
                        {
                            let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                            loadedGame = Gamesave(worldType: self.worldType, playerPositionX: saveGame3.playerPositionX, playerPositionY: saveGame3.playerPositionY)//, saveScreenshot: saveGame3.saveScreenshot!)//, items: saveGame3.items, inventoryItems: saveGame3.inventoryItems)
                            if let view = view
                            {
                                SKTAudio.sharedInstance().playSoundEffect("click.wav")
                                let transition:SKTransition = SKTransition.fade(withDuration: 2)
                                var scene = SKScene()
                                if loadedGame?.worldType == "GameScene"
                                {
                                    scene = GameScene(size: view.bounds.size)
                                }
                                else if loadedGame?.worldType == "House1"
                                {
                                    scene = House1(size: view.bounds.size)
                                }
                                self.view?.presentScene(scene, transition: transition)
                            }
                        }
                    }
                }
                
            }
        }
                
    }
    
    func captureScreen() -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions((self.view?.bounds.size)!, true, 0)
        self.view?.drawHierarchy(in: (self.view?.bounds)!, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        touch = touches.first
        touchLocation = touch!.location(in: cam)
        previousLocation = touch!.previousLocation(in: cam)
        
        if pressedControls[analog] != nil && (analog.intersects(analogDefault) || analog.intersects(analogUp) || analog.intersects(analogDown) || analog.intersects(analogLeft) || analog.intersects(analogRight))
        {
            /*var analogX = analog.position.x + ((touchLocation?.x)! - (previousLocation?.x)!)
             var analogY = analog.position.y + ((touchLocation?.y)! - (previousLocation?.y)!)
             analogX = max(analogX, analog.size.width / 2)
             analogX = min(analogX, size.width - analog.size.width / 2)
             analogY = max(analogY, analog.size.height / 2)
             analogY = min(analogY, size.height - analog.size.height / 2)*/
            
            analog.position = CGPoint(x: (touchLocation?.x)!, y: (touchLocation?.y)!)
            
            /*if !(analogX > self.frame.width / 4 || analogY > self.frame.height / 2.5)
             {
             analog.position = CGPoint(x: analogX, y: analogY)
             }*/
        }
            
        else
        {
            let analogHeight = analog.size.height
            analog.position = CGPoint(x: analogDefault.position.x + analogHeight / 2, y: analogDefault.position.y + analogHeight / 2)
        }
        
        
        
        /*for touch in touches
         {
         let touchLocation = touch.location(in: cam)
         
         if analog.contains(touchLocation)
         {
         return
         }
         if (!analog.contains(touchLocation) && pressedControls[analog] != nil) && !sprintButton.contains(touchLocation)
         {
         pressedControls[analog] = nil
         analog.position = convert(CGPoint(x: self.frame.width / 7, y: self.frame.midY / 2), to: cam)
         pavils.animationIterator = 0
         }
         /*if !sprintButton.contains(touchLocation) && pressedControls[sprintButton] != nil
         {
         pressedControls[sprintButton] = nil
         sprintButton.texture = sprintTexture
         }*/
         
         }*/
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if pavils.stamina < pavils.maxstamina
        {
            pavils.stamina += 0.05
            staminaBar.size = CGSize(width: pavils.stamina, height: 10)
            staminaBar.zPosition = 8
            //cam.addChild(staminaBar)
        }
        
        
        pavils.playingSound = false
        for touch in touches
        {
            let touchLocation = touch.location(in: cam)
            if analog.contains(touchLocation) && pressedControls[analog] != nil
            {
                pressedControls[analog] = nil
                let analogHeight = analog.size.height
                analog.position = CGPoint(x: analogDefault.position.x + analogHeight / 2, y: analogDefault.position.y + analogHeight / 2)
                pavils.animationIterator = 0
                pavils.moving = false
                
            }
            if sprintButton.contains(touchLocation) && pressedControls[sprintButton] != nil
            {
                pressedControls[sprintButton] = nil
                sprintButton.texture = sprintTexture
            }
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        if (contact.bodyA.categoryBitMask == playerCategory) && (contact.bodyB.categoryBitMask == treeCategory)
        {
            print("INTERSECTS")
            pavils.intersects = true
        }
        else
        {
            pavils.intersects = false
        }
    }
    
    //GAMELOOP
    override func update(_ currentTime: TimeInterval)
    {
        fireplace.texture = SKTexture(imageNamed: "fireplace_2000\(fireplaceIterator).png")
        
        fireplaceIterator += 1
        
        if fireplaceIterator > 3
        {
            fireplaceIterator = 0
        }
        
        if questHandler.activeQuest == 0
        {
            switch questHandler.activeQuestStep
            {
            case -1:
                if pavils.hero.intersects(questHandler.quest1Marker)
                {
                    questHandler.quest1Marker.removeFromParent()
                    questHandler.quest1MapMarker.removeFromParent()
                    questHandler.activeQuestStep += 1
                    questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                }
            case 0:
                for item in pavils.items
                {
                    if item.itemUse == "Fire"
                    {
                        questHandler.activeQuestStep += 1
                        questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                    }
                }
            case 1:
                var numberOfWoodIterator = 0
                for item in (ingameMenu?.items)!
                {
                    if item.itemUse == "Fire"
                    {
                        numberOfWoodIterator += 1
                    }
                }
                if numberOfWoodIterator == 3
                {
                    questHandler.quest1Marker.position = CGPoint(x: -158.569, y: 844.411)
                    questHandler.quest1MapMarker.position = CGPoint(x: (((questHandler.quest1Marker.position.x + 4722) / 39) - 84), y: (((questHandler.quest1Marker.position.y + 283) / 41) - 94.5))
                    self.addChild(questHandler.quest1Marker)
                    gameMap.addChild(questHandler.quest1MapMarker)
                    questHandler.activeQuestStep += 1
                    questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                }
            case 2:
                if pavils.hero.intersects(questHandler.quest1Marker)
                {
                    questHandler.quest1Marker.removeFromParent()
                    questHandler.quest1MapMarker.removeFromParent()
                    questHandler.activeQuestStep += 1
                    questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                }
            default:
                break
            }
            
        }
        
        //(width: 168.3, height: 189.3)
        
        
        var hidePickupButton: Bool = true
        for item in pavils.items
        {
            if pavils.hero.intersects(item.itemObject)
            {
                pickUpButton.isHidden = false
                hidePickupButton = false
                break
            }
        }
        
        if hidePickupButton == true
        {
            pickUpButton.isHidden = true
        }
        
        
        
        gameMarker.position = CGPoint(x: (((pavils.hero.position.x + 4722) / 39) - 84), y: (((pavils.hero.position.y + 283) / 41) - 94.5))
        
        var useWaterSound: Bool = false
        
        
        if useWaterSound == true
        {
            if SKTAudio.sharedInstanceWater().backgroundMusicPlayer?.isPlaying == false
            {
                SKTAudio.sharedInstanceWater().playBackgroundMusic("river.mp3")
            }
        }
        else
        {
            if SKTAudio.sharedInstanceWater().backgroundMusicPlayer?.isPlaying == true
            {
                SKTAudio.sharedInstanceWater().pauseBackgroundMusic()
            }
        }
        
        
        if inAttack == true
        {
            if pavils.direction == "UP"
            {
                pavils.hero.texture = pavils.heroAttackTexturesU[Int(pavils.attackAnimIterator / 4)]
            }
            else if pavils.direction == "DOWN"
            {
                pavils.hero.texture = pavils.heroAttackTexturesS[Int(pavils.attackAnimIterator / 4)]
            }
            else if pavils.direction == "LEFT"
            {
                pavils.hero.texture = pavils.heroAttackTexturesA[Int(pavils.attackAnimIterator / 4)]
            }
            else if pavils.direction == "RIGHT"
            {
                pavils.hero.texture = pavils.heroAttackTexturesD[Int(pavils.attackAnimIterator / 4)]
            }
            
            if Int(pavils.attackAnimIterator / 4) == 3
            {
                SKTAudio.sharedInstanceEffects().playSoundEffect("swordSwing.mp3")
            }
            
            pavils.attackAnimIterator += 1
            
            if pavils.attackAnimIterator == 11 * 4 - 1
            {
                pavils.attackAnimIterator = 0
                inAttack = false
            }
        }
        

        if toMenu == true
        {
            for component in (ingameMenu?.menuComponents)!
            {
                component.isHidden = false
            }
            toMenu = false
            inMenu = true
        }
        if inMenu != true
        {
            //CAMERA MOVEMENT
            let playerLocation = CGPoint(x: pavils.hero.frame.midX, y: pavils.hero.frame.midY)
            cam.position = playerLocation
            
            for area in actionAreas
            {
                if area.intersects(pavils.hero)
                {
                    actionButton.isHidden = false
                    break
                }
                actionButton.isHidden = true
            }
            
            //ANALOG CONTROLS IMPLEMENTATION
            if pressedControls[analog] != nil
            {
                pavils.intersects = false
                var movementSpeedConstant: CGFloat = 1.5
                if pressedControls[sprintButton] != nil && pavils.stamina > 5
                {
                    movementSpeedConstant *= 2
                }
                pavils.moving = false
                
                /*for treeCollision in treeCollisions
                 {
                 if pavils.hero.intersects(treeCollision) == true
                 {
                 pavils.intersects = true
                 break
                 }
                 }*/
                
                /*func outOfBounds() -> Bool
                 {
                 if pavils.hero.position.x < 1910.65 && pavils.hero.position.x > -4722.29 && pavils.hero.position.y > 283.194 && pavils.hero.position.y < 7506.55
                 {
                 return false
                 }
                 else
                 {
                 return true
                 }
                 }
                 */
                if pavils.intersects == false && pressedControls[analog] != nil
                {
                    print("PLAYERPOSITION: x:\(pavils.hero.position.x) y:\(pavils.hero.position.y)")
                    //RIGHT
                    if analog.intersects(analogRight)
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x + movementSpeedConstant, y: pavils.hero.position.y)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesR[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "RIGHT"
                    }
                    //LEFT
                    if analog.intersects(analogLeft)
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x - movementSpeedConstant, y: pavils.hero.position.y)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesW[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "LEFT"
                    }
                    //UP
                    if analog.intersects(analogUp)
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x, y: pavils.hero.position.y + movementSpeedConstant)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesU[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "UP"
                    }
                    //DOWN
                    if analog.intersects(analogDown)
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x, y: pavils.hero.position.y - movementSpeedConstant)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesD[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "DOWN"
                    }
                    if pavils.playingSound == false
                    {
                        SKTAudio.sharedInstanceEffects().playBackgroundMusic("walking.mp3")
                        pavils.playingSound = true
                    }
                }
                
                pavils.animationIterator += 1
                if pavils.animationIterator == 11 * 4 - 1
                {
                    pavils.animationIterator = 0
                }
            }
            else
            {
                SKTAudio.sharedInstanceEffects().pauseBackgroundMusic()
            }
            
            //PLAYER VITALITY AND STAMINA BARS UI IMPLEMENTATION
            
            //VITALITY BAR
            
            //STAMINA BAR
            if pavils.moving == true && pressedControls[sprintButton] != nil && pavils.stamina > 0
            {
                pavils.stamina -= 0.25
                staminaBar.size = CGSize(width: pavils.stamina, height: 10)
                //cam.addChild(staminaBar)
            }
            if pavils.stamina < pavils.maxstamina && !(pavils.moving == true && pressedControls[sprintButton] != nil)
            {
                pavils.stamina += 0.05
                staminaBar.size = CGSize(width: pavils.stamina, height: 10)
                //cam.addChild(staminaBar)
            }
            
            
        }
    }
}

