//
//  GameViewController.swift
//  PavilsGame
//
//  Created by Vojtech Florko on 30/12/2016.
//  Copyright © 2016 Vojtech Florko. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController
{
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var presentsTitle: UILabel!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        //let scene = GameScene(size: view.bounds.size)
        let scene = Menu(size: view.bounds.size)
        companyName.isHidden = true
        presentsTitle.isHidden = true
        let skView = view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .resizeFill
        skView.presentScene(scene)
    }

    override var shouldAutorotate: Bool
    {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask
    {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            return .allButUpsideDown
        }
        else
        {
            return .all
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    override var prefersStatusBarHidden: Bool
    {
        return true
    }
}
