Project name: Lineage of Knights

Team: Pocket RPG Studio

Short Project Description: 
The main targeted platform for our RPG game is iOS. It is set in the medieval times, where Pavils, former fierce warrior and mercenary, 
finds himself hiding from death in the forest. After slowly exploring the nature around, building a shelter and crafting weapons, he 
finally dares to leave the forest to see, what else has life prepared for him...

Team members: Vojtech Florko, Dominika Filipova

Contact: vitoflorko@icloud.com, filipova.nika@gmail.com
