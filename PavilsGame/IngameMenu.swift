//
//  IngameMenu.swift
//  PavilsGame
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class IngameMenu
{
    //GENERAL MENU COMPONENTS
    var menuBackground = SKSpriteNode(imageNamed: "ingameMenuPaper.png")
    var menuBorder = SKShapeNode()
    var menuButton1 = SKShapeNode()
    var menuButton2 = SKShapeNode()
    var menuButton3 = SKShapeNode()
    var menuButton1Text = SKLabelNode(fontNamed: "Georgia")
    var menuButton2Text = SKLabelNode(fontNamed: "Georgia")
    var menuButton3Text = SKLabelNode(fontNamed: "Georgia")
    var exitButtonX = SKSpriteNode(imageNamed: "exitButtonX.png")
    
    //SKILLS SECTION

    //INVENTORY SECTION
    var pavilsCharacter = SKSpriteNode(imageNamed: "heroD4.png")
    var pavilsName = SKLabelNode(fontNamed: "Georgia")
    var topAttireName = SKLabelNode(fontNamed: "Georgia")
    var bottomAttireName = SKLabelNode(fontNamed: "Georgia")
    var topAttireSlot = SKShapeNode()
    var bottomAttireSlot = SKShapeNode()
    
    var pavilsArmor = SKSpriteNode(imageNamed: "steelPlatedArmor.png")
    var pavilsGraves = SKSpriteNode(imageNamed: "steelPlatedGraves.png")
    
    var itemsName = SKLabelNode(fontNamed: "Georgia")
    
    var itemSlot1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
    var itemSlot2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
    var itemSlot3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
    var itemSlot4 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
    var itemSlot5 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
    var itemSlot6 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
    
    var itemSlots: [SKShapeNode] = []
    
    var items: [Item] = []
    
    
    //GAME MENU SECTION
    var gameMenuSaveButtonText = SKLabelNode(fontNamed: "Georgia")
    var gameMenuLoadButtonText = SKLabelNode(fontNamed: "Georgia")
    var gameMenuExitButtonText = SKLabelNode(fontNamed: "Georgia")
    
    //SAVE GAME SECTION
    var savePosition1 = SKShapeNode()
    var savePosition2 = SKShapeNode()
    var savePosition3 = SKShapeNode()
    var savePosition1Text = SKLabelNode(fontNamed: "Georgia")
    var savePosition2Text = SKLabelNode(fontNamed: "Georgia")
    var savePosition3Text = SKLabelNode(fontNamed: "Georgia")
    var savePosition1Image = SKSpriteNode()
    var savePosition2Image = SKSpriteNode()
    var savePosition3Image = SKSpriteNode()
    
    //LOAD GAME SECTION
    var loadPosition1 = SKShapeNode()
    var loadPosition2 = SKShapeNode()
    var loadPosition3 = SKShapeNode()
    var loadPosition1Text = SKLabelNode(fontNamed: "Georgia")
    var loadPosition2Text = SKLabelNode(fontNamed: "Georgia")
    var loadPosition3Text = SKLabelNode(fontNamed: "Georgia")
    var loadPosition1Image = SKSpriteNode()
    var loadPosition2Image = SKSpriteNode()
    var loadPosition3Image = SKSpriteNode()
    
    var selfViewWidth: CGFloat = 0.0
    var selfViewHeight: CGFloat = 0.0
    
    var menuComponents: [SKNode] = []
    var submenuComponents: [[SKNode]] = [[]]

    var menu1Components: [SKNode] = []
    var menu2Components: [SKNode] = []
    var menu3Components: [SKNode] = []
    
    var saveComponents: [SKNode] = []
    var loadComponents: [SKNode] = []
    
    var savingGame: Bool = false
    var loadingGame: Bool = false
    
    init(selfViewWidth: CGFloat, selfViewHeight: CGFloat)
    {
        self.selfViewWidth = selfViewWidth
        self.selfViewHeight = selfViewHeight
        
        exitButtonX.zPosition = 14
        
        menuBackground.position = CGPoint(x: 0, y: 0)
        menuBackground.size = CGSize(width: selfViewWidth + 160, height: selfViewHeight)
        menuBackground.zPosition = 13
        
        menuButton1Text.text = "Skills"
        menuButton2Text.text = "Inventory"
        menuButton3Text.text = "Game menu"
        menuButton1Text.fontColor = UIColor.white
        menuButton2Text.fontColor = UIColor.white
        menuButton3Text.fontColor = UIColor.white
        menuButton1Text.zPosition = 14
        menuButton2Text.zPosition = 14
        menuButton3Text.zPosition = 14
        menuButton1Text.fontSize = 20
        menuButton2Text.fontSize = 20
        menuButton3Text.fontSize = 20
        
        gameMenuSaveButtonText.text = "SAVE"
        gameMenuLoadButtonText.text = "LOAD"
        gameMenuExitButtonText.text = "EXIT"
        gameMenuSaveButtonText.fontColor = UIColor.white
        gameMenuLoadButtonText.fontColor = UIColor.white
        gameMenuExitButtonText.fontColor = UIColor.white
        gameMenuSaveButtonText.zPosition = 14
        gameMenuLoadButtonText.zPosition = 14
        gameMenuExitButtonText.zPosition = 14
        gameMenuSaveButtonText.fontSize = 30
        gameMenuLoadButtonText.fontSize = 30
        gameMenuExitButtonText.fontSize = 30

        savePosition1Text.fontColor = UIColor.white
        savePosition2Text.fontColor = UIColor.white
        savePosition3Text.fontColor = UIColor.white
        savePosition1Text.zPosition = 14
        savePosition2Text.zPosition = 14
        savePosition3Text.zPosition = 14
        savePosition1Text.fontSize = 10
        savePosition2Text.fontSize = 10
        savePosition3Text.fontSize = 10
        savePosition1Image.zPosition = 15
        savePosition2Image.zPosition = 15
        savePosition3Image.zPosition = 15
        
        loadPosition1Text.fontColor = UIColor.white
        loadPosition2Text.fontColor = UIColor.white
        loadPosition3Text.fontColor = UIColor.white
        loadPosition1Text.zPosition = 14
        loadPosition2Text.zPosition = 14
        loadPosition3Text.zPosition = 14
        loadPosition1Text.fontSize = 10
        loadPosition2Text.fontSize = 10
        loadPosition3Text.fontSize = 10
        loadPosition1Image.zPosition = 15
        loadPosition2Image.zPosition = 15
        loadPosition3Image.zPosition = 15
        
        menuBorder = SKShapeNode(rect: CGRect(x: 0, y: 0, width: selfViewWidth - 220, height: selfViewHeight - 100))
        menuButton1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 40))
        menuButton2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 40))
        menuButton3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 40))
        
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            savePosition1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 80))
            savePosition2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 80))
            savePosition3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 80))
            
            savePosition1Image.size = savePosition1.frame.size
            savePosition2Image.size = savePosition2.frame.size
            savePosition3Image.size = savePosition3.frame.size
            
            loadPosition1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 80))
            loadPosition2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 80))
            loadPosition3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (selfViewWidth - 220) / 3, height: 80))
            
            loadPosition1Image.size = loadPosition1.frame.size
            loadPosition2Image.size = loadPosition2.frame.size
            loadPosition3Image.size = loadPosition3.frame.size
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            savePosition1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((selfViewWidth) / 2) - 220) / 3, height: 80))
            savePosition2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((selfViewWidth) / 2) - 220) / 3, height: 80))
            savePosition3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((selfViewWidth) / 2) - 220) / 3, height: 80))
            
            savePosition1Image.size = savePosition1.frame.size
            savePosition2Image.size = savePosition2.frame.size
            savePosition3Image.size = savePosition3.frame.size
            
            loadPosition1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((selfViewWidth) / 2) - 220) / 3, height: 80))
            loadPosition2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((selfViewWidth) / 2) - 220) / 3, height: 80))
            loadPosition3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((selfViewWidth) / 2) - 220) / 3, height: 80))
            
            loadPosition1Image.size = loadPosition1.frame.size
            loadPosition2Image.size = loadPosition2.frame.size
            loadPosition3Image.size = loadPosition3.frame.size
        }
        /*else if UIDevice.current.userInterfaceIdiom == .pad
        {
            menuBorder = SKShapeNode(rect: CGRect(x: 0, y: 0, width: selfViewWidth - 320, height: selfViewHeight - 100))
        }*/
        menuBorder.lineWidth = 2
        menuBorder.strokeColor = UIColor.black
        menuBorder.zPosition = 14
        
        menuButton1.zPosition = 14
        menuButton2.zPosition = 14
        menuButton3.zPosition = 14
        menuButton1.strokeColor = UIColor.black
        menuButton2.strokeColor = UIColor.black
        menuButton3.strokeColor = UIColor.black
        menuButton1.lineWidth = 2
        menuButton2.lineWidth = 2
        menuButton3.lineWidth = 2

        pavilsName.fontColor = UIColor.yellow
        pavilsName.fontSize = 20
        pavilsName.text = "Pavils"
        pavilsName.zPosition = 14
        
        topAttireName.fontColor = UIColor.black
        bottomAttireName.fontColor = UIColor.black
        topAttireName.zPosition = 14
        bottomAttireName.zPosition = 14
        topAttireName.fontSize = 20
        bottomAttireName.fontSize = 20
        topAttireName.text = "Top attire"
        bottomAttireName.text = "Bottom attire"
        
        topAttireSlot = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
        bottomAttireSlot = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 80, height: 80))
            
        topAttireSlot.zPosition = 14
        bottomAttireSlot.zPosition = 14
        topAttireSlot.strokeColor = UIColor.black
        bottomAttireSlot.strokeColor = UIColor.black
        topAttireSlot.lineWidth = 2
        bottomAttireSlot.lineWidth = 2

        pavilsCharacter.size = CGSize(width: 150, height: 150)
        pavilsCharacter.zPosition = 14
        
        savePosition1.zPosition = 14
        savePosition2.zPosition = 14
        savePosition3.zPosition = 14
        savePosition1.strokeColor = UIColor.black
        savePosition2.strokeColor = UIColor.black
        savePosition3.strokeColor = UIColor.black
        savePosition1.lineWidth = 2
        savePosition2.lineWidth = 2
        savePosition3.lineWidth = 2
        
        loadPosition1.zPosition = 14
        loadPosition2.zPosition = 14
        loadPosition3.zPosition = 14
        loadPosition1.strokeColor = UIColor.black
        loadPosition2.strokeColor = UIColor.black
        loadPosition3.strokeColor = UIColor.black
        loadPosition1.lineWidth = 2
        loadPosition2.lineWidth = 2
        loadPosition3.lineWidth = 2
        
        
        
        var image = UIImage(named: "emptyPic.png")
        if let pic = image?.scaleImage(toSize: CGSize(width: savePosition1Image.frame.width, height: savePosition1Image.frame.height))
        {
            image = pic
        }
        let texture = SKTexture(image: image!)
        savePosition1Image.texture = texture
        savePosition2Image.texture = texture
        savePosition3Image.texture = texture
        
        loadPosition1Image.texture = texture
        loadPosition2Image.texture = texture
        loadPosition3Image.texture = texture
        
        menuComponents.append(menuBackground)
        menuComponents.append(menuBorder)
        menuComponents.append(menuButton1)
        menuComponents.append(menuButton2)
        menuComponents.append(menuButton3)
        menuComponents.append(exitButtonX)
        menuComponents.append(menuButton1Text)
        menuComponents.append(menuButton2Text)
        menuComponents.append(menuButton3Text)
        
        menu2Components.append(pavilsName)
        menu2Components.append(pavilsCharacter)
        menu2Components.append(topAttireName)
        menu2Components.append(topAttireSlot)
        menu2Components.append(bottomAttireName)
        menu2Components.append(bottomAttireSlot)
        
        menu3Components.append(gameMenuSaveButtonText)
        menu3Components.append(gameMenuLoadButtonText)
        menu3Components.append(gameMenuExitButtonText)
        
        saveComponents.append(savePosition1)
        saveComponents.append(savePosition2)
        saveComponents.append(savePosition3)
        saveComponents.append(savePosition1Text)
        saveComponents.append(savePosition2Text)
        saveComponents.append(savePosition3Text)
        saveComponents.append(savePosition1Image)
        saveComponents.append(savePosition2Image)
        saveComponents.append(savePosition3Image)
        
        loadComponents.append(loadPosition1)
        loadComponents.append(loadPosition2)
        loadComponents.append(loadPosition3)
        loadComponents.append(loadPosition1Text)
        loadComponents.append(loadPosition2Text)
        loadComponents.append(loadPosition3Text)
        loadComponents.append(loadPosition1Image)
        loadComponents.append(loadPosition2Image)
        loadComponents.append(loadPosition3Image)
        
        submenuComponents.append(menu1Components)
        submenuComponents.append(menu2Components)
        submenuComponents.append(menu3Components)
        
        
        pavilsArmor.size = CGSize(width: 80, height: 80)
        pavilsArmor.zPosition = 14
        pavilsArmor.position = CGPoint(x: (topAttireSlot.position.x) + (topAttireSlot.frame.width) / 2, y: (topAttireSlot.position.y) + (topAttireSlot.frame.height) / 2)
        menu2Components.append(pavilsArmor)
        
        
        pavilsGraves.size = CGSize(width: 80, height: 80)
        pavilsGraves.zPosition = 14
        pavilsGraves.position = CGPoint(x: (bottomAttireSlot.position.x) + (bottomAttireSlot.frame.width) / 2, y: (bottomAttireSlot.position.y) + (bottomAttireSlot.frame.height) / 2)
        menu2Components.append(pavilsGraves)
        
        itemsName.fontColor = UIColor.yellow
        itemsName.zPosition = 14
        itemsName.text = "Items (tap to throw out)"
        itemsName.fontSize = 20
        menu2Components.append(itemsName)
        
        itemSlots.append(itemSlot1)
        itemSlots.append(itemSlot2)
        itemSlots.append(itemSlot3)
        itemSlots.append(itemSlot4)
        itemSlots.append(itemSlot5)
        itemSlots.append(itemSlot6)

        
        for slot in itemSlots
        {
            slot.strokeColor = UIColor.black
            slot.lineWidth = 2
            slot.zPosition = 14
            menu2Components.append(slot)
        }
    }
    
    func hideAllMenuComponents()
    {
        for component in menuComponents
        {
            component.isHidden = true
        }
        for component in menu2Components
        {
            component.isHidden = true
        }
        for component in menu3Components
        {
            component.isHidden = true
        }
        hideMenu3Subcomponents()
    }
    
    func hideMenu3Subcomponents()
    {
        for component in saveComponents
        {
            component.isHidden = true
        }
        for component in loadComponents
        {
            component.isHidden = true
        }
    }
    
    func hidePlayerItems()
    {
        for item in items
        {
            item.itemObject.isHidden = true
        }
    }
    
    func showPlayerItems()
    {
        for item in items
        {
            item.itemObject.isHidden = false
        }
    }
    
    func hideMenu2Subcomponents()
    {
        for component in menu2Components
        {
            component.isHidden = true
        }
    }
    
    func showMenu1Components()
    {
        for array in submenuComponents
        {
            for component in array
            {
                component.isHidden = true
            }
        }
        for component in menu1Components
        {
            component.isHidden = false
        }
        hideMenu3Subcomponents()
    }
    
    func showMenu2Components()
    {
        for array in submenuComponents
        {
            for component in array
            {
                component.isHidden = true
            }
        }
        for component in menu2Components
        {
            component.isHidden = false
        }
        hideMenu3Subcomponents()
    }
    
    func showMenu3Components()
    {
        for array in submenuComponents
        {
            for component in array
            {
                component.isHidden = true
            }
        }
        for component in menu3Components
        {
            component.isHidden = false
        }
        hideMenu3Subcomponents()
    }
    
    func showSaveSubmenuComponents()
    {
        hideMenu3Subcomponents()
        for component in saveComponents
        {
            component.isHidden = false
        }
    }
    
    func showLoadSubmenuComponents()
    {
        hideMenu3Subcomponents()
        for component in loadComponents
        {
            component.isHidden = false
        }
    }
    
    
    
}
