//
//  Menu.swift
//  PavilsGame
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import SpriteKit
import GameplayKit
import Darwin

class Menu: SKScene, SKPhysicsContactDelegate
{
    //TOUCH CONTROLS
    var touch: UITouch? = nil
    var touchLocation: CGPoint? = nil
    var previousLocation: CGPoint? = nil
    var pressedControls = [SKSpriteNode: UITouch]()
    
    //MENU GRAPHICS
    var menuWallpaper = SKSpriteNode()
    var title = SKLabelNode()
    var newGameButton = SKLabelNode()
    var loadGameButton = SKLabelNode()
    var exitGameButton = SKLabelNode()
    let sword = SKSpriteNode(imageNamed: "velkorodKnightSword.png")
    var swordAlphaSwitch: Bool = false
    
    //LOAD GAME SECTION
    var loadPosition1 = SKShapeNode()
    var loadPosition2 = SKShapeNode()
    var loadPosition3 = SKShapeNode()
    var loadPosition1Text = SKLabelNode(fontNamed: "Georgia")
    var loadPosition2Text = SKLabelNode(fontNamed: "Georgia")
    var loadPosition3Text = SKLabelNode(fontNamed: "Georgia")
    var loadPosition1Image = SKSpriteNode()
    var loadPosition2Image = SKSpriteNode()
    var loadPosition3Image = SKSpriteNode()
    var loadComponents: [SKNode] = []
    var loading: Bool = false
    
    enum UIUserInterfaceIdiom : Int
    {
        case unspecified
        case phone // iPhone and iPod touch style UI
        case pad // iPad style UI
    }

    
    override func didMove(to view: SKView)
    {
        loadedGame = Gamesave(worldType: "GameScene", playerPositionX: self.frame.width / 2, playerPositionY: self.frame.height / 2)//, saveScreenshot: UIImage(named: "emptyPic.png")!)//, items: [], inventoryItems: [])
        
        //WALLPAPER
        self.backgroundColor = UIColor.black
        menuWallpaper = SKSpriteNode(imageNamed: "menuPaper")
        menuWallpaper.position = CGPoint(x: (self.view?.frame.width)! / 2, y: (self.view?.frame.height)! / 2)
        menuWallpaper.size = CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!)
        menuWallpaper.alpha = 0.0
        menuWallpaper.zPosition = 2
        self.addChild(menuWallpaper)
        
        //GAME TITLE
        title = SKLabelNode(text: "Lineage of Knights")
        title.fontName = "SavoyeLetPlain"
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            title.fontSize = 65.0
            title.position = CGPoint(x: 200, y: frame.size.height - 70)
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            title.fontSize = 100.0
            title.position = CGPoint(x: 300, y: frame.size.height - 100)
        }
        
        title.fontColor = UIColor.black
        title.zPosition = 3
        
        //NEW GAME BUTTON
        newGameButton = SKLabelNode(text: "New game")
        newGameButton.fontName = "SavoyeLetPlain"
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            newGameButton.fontSize = 45.0
            newGameButton.position = CGPoint(x: 100, y: frame.size.height - 130)
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            newGameButton.fontSize = 65.0
            newGameButton.position = CGPoint(x: 300, y: frame.size.height - 230)
        }

        newGameButton.fontColor = UIColor.black
        newGameButton.zPosition = 3
        
        //LOAD GAME BUTTON
        loadGameButton = SKLabelNode(text: "Load game")
        loadGameButton.fontName = "SavoyeLetPlain"
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            loadGameButton.fontSize = 45.0
            loadGameButton.position = CGPoint(x: 100, y: frame.size.height - 190)
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            loadGameButton.fontSize = 65.0
            loadGameButton.position = CGPoint(x: 300, y: frame.size.height - 290)
        }
        
        loadGameButton.fontColor = UIColor.black
        loadGameButton.zPosition = 3
        
        //EXIT GAME BUTTON
        exitGameButton = SKLabelNode(text: "Exit game")
        exitGameButton.fontName = "SavoyeLetPlain"
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            exitGameButton.fontSize = 45.0
            exitGameButton.position = CGPoint(x: 100, y: frame.size.height - 250)
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            exitGameButton.fontSize = 65.0
            exitGameButton.position = CGPoint(x: 300, y: frame.size.height - 350)
        }
        
        exitGameButton.fontColor = UIColor.black
        exitGameButton.zPosition = 3
        
        loadPosition1Text.color = UIColor.black
        loadPosition2Text.color = UIColor.black
        loadPosition3Text.color = UIColor.black
        loadPosition1Text.zPosition = 7
        loadPosition2Text.zPosition = 7
        loadPosition3Text.zPosition = 7
        loadPosition1Text.fontSize = 10
        loadPosition2Text.fontSize = 10
        loadPosition3Text.fontSize = 10
        loadPosition1Image.zPosition = 7
        loadPosition2Image.zPosition = 7
        loadPosition3Image.zPosition = 7
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            loadPosition1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: ((self.view?.frame.width)! - 220) / 3, height: 80))
            loadPosition2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: ((self.view?.frame.width)! - 220) / 3, height: 80))
            loadPosition3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: ((self.view?.frame.width)! - 220) / 3, height: 80))
            
            loadPosition1Image.size = loadPosition1.frame.size
            loadPosition2Image.size = loadPosition2.frame.size
            loadPosition3Image.size = loadPosition3.frame.size
        }
        
        if UIDevice.current.userInterfaceIdiom == .pad
        {
            loadPosition1 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((self.view?.frame.width)! / 2) - 220) / 3, height: 80))
            loadPosition2 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((self.view?.frame.width)! / 2) - 220) / 3, height: 80))
            loadPosition3 = SKShapeNode(rect: CGRect(x: 0, y: 0, width: (((self.view?.frame.width)! / 2) - 220) / 3, height: 80))
            
            loadPosition1Image.size = loadPosition1.frame.size
            loadPosition2Image.size = loadPosition2.frame.size
            loadPosition3Image.size = loadPosition3.frame.size
        }

        
        loadPosition1.zPosition = 7
        loadPosition2.zPosition = 7
        loadPosition3.zPosition = 7
        loadPosition1.strokeColor = UIColor.black
        loadPosition2.strokeColor = UIColor.black
        loadPosition3.strokeColor = UIColor.black
        loadPosition1.lineWidth = 2
        loadPosition2.lineWidth = 2
        loadPosition3.lineWidth = 2
        
        var image = UIImage(named: "emptyPic.png")
        if let pic = image?.scaleImage(toSize: CGSize(width: loadPosition1Image.frame.width, height: loadPosition1Image.frame.height))
        {
            image = pic
        }
        
        let texture = SKTexture(image: image!)
        
        loadPosition1Image.texture = texture
        loadPosition2Image.texture = texture
        loadPosition3Image.texture = texture
        
        loadPosition1.position = CGPoint(x: newGameButton.position.x
            + 150, y: newGameButton.position.y - 40)
        loadPosition2.position = CGPoint(x: newGameButton.position.x + 150, y: newGameButton.position.y - 130)
        loadPosition3.position = CGPoint(x: newGameButton.position.x + 150, y: newGameButton.position.y - 220)
        
        loadPosition1Image.position = CGPoint(x: newGameButton.position.x
            + 150 + (loadPosition1Image.frame.width / 2), y: newGameButton.position.y - 40 + (loadPosition1Image.frame.height / 2))
        loadPosition2Image.position = CGPoint(x: newGameButton.position.x + 150 + (loadPosition2Image.frame.width / 2), y: newGameButton.position.y - 130 + (loadPosition2Image.frame.height / 2))
        loadPosition3Image.position = CGPoint(x: newGameButton.position.x + 150 + (loadPosition3Image.frame.width / 2), y: newGameButton.position.y - 220 + (loadPosition3Image.frame.height / 2))
        
        loadComponents.append(loadPosition1)
        loadComponents.append(loadPosition2)
        loadComponents.append(loadPosition3)
        loadComponents.append(loadPosition1Text)
        loadComponents.append(loadPosition2Text)
        loadComponents.append(loadPosition3Text)
        loadComponents.append(loadPosition1Image)
        loadComponents.append(loadPosition2Image)
        loadComponents.append(loadPosition3Image)
        
        for component in self.loadComponents
        {
            component.isHidden = true
        }
    }
    
    //TOUCH IMPLEMENTATION
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if let touch = touches.first
        {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if node == self.newGameButton
            {
                SKTAudio.sharedInstanceEffects().playSoundEffect("click.wav")
                
                let overlayView = SKShapeNode(rectOf: CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!))
                overlayView.position = CGPoint(x: 0, y: 0)
                overlayView.zPosition = 9
                overlayView.strokeColor = UIColor.black
                overlayView.fillColor = UIColor.black
                self.addChild(overlayView)
                
                self.addChild(sword)
                sword.position = CGPoint(x: 0, y: 0)
                sword.zPosition = 10
                
                _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: Selector("loadingAnimator"), userInfo: nil, repeats: true)
                
                newGame = true
                self.newGameButton.color = UIColor.gray
                
                
                if let view = view
                {
                    
                    
                    let transition:SKTransition = SKTransition.fade(withDuration: 2)
                    let scene: SKScene = GameScene(size: view.bounds.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            }
            if node == self.loadGameButton
            {
                SKTAudio.sharedInstanceEffects().playSoundEffect("click.wav")
                for component in self.loadComponents
                {
                    component.isHidden = false
                }
                loading = true
            }
            if loading == true
            {
                let userDefaults = UserDefaults.standard
                if let data = userDefaults.object(forKey: "savePosition1") as? Data
                {
                    let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                    let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame1")!)
                    loadPosition1Image.texture = saveGameTexture
                }
                if let data = userDefaults.object(forKey: "savePosition2") as? Data
                {
                    let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                    let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame2")!)
                    loadPosition2Image.texture = saveGameTexture
                }
                if let data = userDefaults.object(forKey: "savePosition3") as? Data
                {
                    let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                    let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame3")!)
                    loadPosition3Image.texture = saveGameTexture
                }
                
                if node == self.loadPosition1Image
                {
                    if let data = userDefaults.object(forKey: "savePosition1") as? Data
                    {
                        let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        loadedGame = Gamesave(worldType: saveGame1.worldType, playerPositionX: saveGame1.playerPositionX, playerPositionY: saveGame1.playerPositionY)//, saveScreenshot: saveGame1.saveScreenshot!)//, items: saveGame1.items)//, inventoryItems: saveGame1.inventoryItems)
                        if let view = view
                        {
                            let overlayView = SKShapeNode(rectOf: CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!))
                            overlayView.position = CGPoint(x: 0, y: 0)
                            overlayView.zPosition = 6
                            overlayView.strokeColor = UIColor.black
                            overlayView.fillColor = UIColor.black
                            self.addChild(overlayView)
                            
                            self.addChild(sword)
                            sword.position = CGPoint(x: 0, y: 0)
                            sword.zPosition = 10
                            
                            _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: Selector("loadingAnimator"), userInfo: nil, repeats: true)
                            
                            
                            let transition:SKTransition = SKTransition.fade(withDuration: 2)
                            let scene: SKScene = GameScene(size: view.bounds.size)
                            self.view?.presentScene(scene, transition: transition)
                        }
                    }
                }
                if node == self.loadPosition2Image
                {
                    if let data = userDefaults.object(forKey: "savePosition2") as? Data
                    {
                        let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        loadedGame = Gamesave(worldType: saveGame2.worldType, playerPositionX: saveGame2.playerPositionX, playerPositionY: saveGame2.playerPositionY)//, saveScreenshot: saveGame2.saveScreenshot!)//, items: saveGame2.items, inventoryItems: saveGame2.inventoryItems)
                        if let view = view
                        {
                            let overlayView = SKShapeNode(rectOf: CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!))
                            overlayView.position = CGPoint(x: 0, y: 0)
                            overlayView.zPosition = 6
                            overlayView.strokeColor = UIColor.black
                            overlayView.fillColor = UIColor.black
                            self.addChild(overlayView)
                            
                            self.addChild(sword)
                            sword.position = CGPoint(x: 0, y: 0)
                            sword.zPosition = 10
                            
                            _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: Selector("loadingAnimator"), userInfo: nil, repeats: true)
                            
                            SKTAudio.sharedInstanceEffects().playSoundEffect("click.wav")
                            let transition:SKTransition = SKTransition.fade(withDuration: 2)
                            let scene: SKScene = GameScene(size: view.bounds.size)
                            self.view?.presentScene(scene, transition: transition)
                        }
                    }
                }
                if node == self.loadPosition3Image
                {
                    if let data = userDefaults.object(forKey: "savePosition3") as? Data
                    {
                        let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        loadedGame = Gamesave(worldType: saveGame3.worldType, playerPositionX: saveGame3.playerPositionX, playerPositionY: saveGame3.playerPositionY)//, saveScreenshot: saveGame3.saveScreenshot!)//, items: saveGame3.items, inventoryItems: saveGame3.inventoryItems)
                        if let view = view
                        {
                            let overlayView = SKShapeNode(rectOf: CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!))
                            overlayView.position = CGPoint(x: 0, y: 0)
                            overlayView.zPosition = 6
                            overlayView.strokeColor = UIColor.black
                            overlayView.fillColor = UIColor.black
                            self.addChild(overlayView)
                            
                            self.addChild(sword)
                            sword.position = CGPoint(x: 0, y: 0)
                            sword.zPosition = 10
                            
                            _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: Selector("loadingAnimator"), userInfo: nil, repeats: true)
                            
                            SKTAudio.sharedInstanceEffects().playSoundEffect("click.wav")
                            let transition:SKTransition = SKTransition.fade(withDuration: 2)
                            let scene: SKScene = GameScene(size: view.bounds.size)
                            self.view?.presentScene(scene, transition: transition)
                        }
                    }
                }

            }
            if node == self.exitGameButton
            {
                exit(0)
            }
        }
        
    }
    
    func loadingAnimator()
    {
        if sword.alpha == 1
        {
            swordAlphaSwitch = false
        }
        else if sword.alpha == 0
        {
            swordAlphaSwitch = true
        }
        
        if swordAlphaSwitch == false
        {
            sword.alpha -= 0.1
        }
        else
        {
            sword.alpha += 0.1
        }
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        touch = touches.first
        touchLocation = touch!.location(in: self)
        previousLocation = touch!.previousLocation(in: self)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        for touch in touches
        {
            let touchLocation = touch.location(in: self)
        }
    }
    
    
    //GAMELOOP
    override func update(_ currentTime: TimeInterval)
    {
        if menuWallpaper.alpha < 1.0
        {
            UIView.animate(withDuration: 1)
            {
                self.menuWallpaper.alpha = 1.0
                self.addChild(self.title)
                self.addChild(self.newGameButton)
                self.addChild(self.loadGameButton)
                self.addChild(self.exitGameButton)
                
                for component in self.loadComponents
                {
                    self.addChild(component)
                }
            }
        }
        
    }
    
}
