//
//  Item.swift
//  PavilsGame
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class Item: NSObject, NSCoding
{
    var itemObject: SKSpriteNode
    var itemInInventory: Bool
    var itemUse: String
    
    init(itemIcon: String, itemInInventory: Bool, itemUse: String, itemPositionX: CGFloat, itemPositionY: CGFloat)
    {
        self.itemObject = SKSpriteNode(imageNamed: itemIcon)
        self.itemObject.zPosition = 8
        self.itemObject.position = CGPoint(x: itemPositionX, y: itemPositionY)
        self.itemInInventory = itemInInventory
        self.itemUse = itemUse
        
    }
    
    init(itemObj: SKSpriteNode, itemInInv: Bool, itemUs: String)
    {
        self.itemObject = itemObj
        self.itemInInventory = itemInInv
        self.itemUse = itemUs
    }
    
    //NSCoding protocol methods
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.itemObject, forKey: "itemObject")
        aCoder.encode(self.itemInInventory, forKey: "itemInInventory")
        aCoder.encode(self.itemUse, forKey: "itemUse")
    }
    
    required convenience init(coder decoder: NSCoder)
    {
        var itemObj: SKSpriteNode? = nil
        var itemInInv: Bool = false
        var itemUs: String = ""
        
        if let itemO = decoder.decodeObject(forKey: "itemObject") as? SKSpriteNode
        {
            itemObj = itemO
        }
        
        if let itemI = decoder.decodeObject(forKey: "itemInInventory") as? Bool
        {
            itemInInv = itemI
        }
        
        if let itemU = decoder.decodeObject(forKey: "itemUse") as? String
        {
            itemUs = itemU
        }
        
        self.init(itemObj: itemObj!, itemInInv: itemInInv, itemUs: itemUs)

    }
    
}
