//
//  Hero.swift
//  PavilsGame
//
//  Created by Vojtech Florko on 01/01/2017.
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class Hero
{
    //NODES
    var hero: SKSpriteNode
    
    //PROPERTIES
    var vitality: Double
    var stamina: Double
    var maxvitality: Double
    var maxstamina: Double
    var animationIterator: Int
    var moving: Bool
    var intersects: Bool
    var playingSound: Bool
    var direction: String
    var attackAnimIterator: Int
    var numberOfItemSlots: Int
    var numberOfItems: Int
    
    //ITEMS
    var items: [Item] = []
    
    //TEXTURES
    var heroTexturesU: [SKTexture] = []
    var heroTexturesD: [SKTexture] = []
    var heroTexturesW: [SKTexture] = []
    var heroTexturesR: [SKTexture] = []
    
    var heroAttackTexturesU: [SKTexture] = []
    var heroAttackTexturesD: [SKTexture] = []
    var heroAttackTexturesS: [SKTexture] = []
    var heroAttackTexturesA: [SKTexture] = []

    init(vitality: Double, stamina: Double, maxvitality: Double, maxstamina: Double)
    {
        //HERO TEXTURES LOADING
        for i in 0..<13
        {
            let heroU = SKTexture(imageNamed: "heroU\(i).png")
            heroTexturesU.append(heroU)
            
            let heroD = SKTexture(imageNamed: "heroD\(i).png")
            heroTexturesD.append(heroD)
            
            let heroW = SKTexture(imageNamed: "heroW\(i).png")
            heroTexturesW.append(heroW)
            
            let heroR = SKTexture(imageNamed: "heroR\(i).png")
            heroTexturesR.append(heroR)
        }
        
        for i in (0...10).reversed()
        {
            let heroAttackU = SKTexture(imageNamed: "heroAttackU\(i).png")
            heroAttackTexturesU.append(heroAttackU)
            
            let heroAttackS = SKTexture(imageNamed: "heroAttackS\(i).png")
            heroAttackTexturesS.append(heroAttackS)
            
            let heroAttackD = SKTexture(imageNamed: "heroAttackD\(i).png")
            heroAttackTexturesD.append(heroAttackD)
            
            let heroAttackA = SKTexture(imageNamed: "heroAttackA\(i).png")
            heroAttackTexturesA.append(heroAttackA)
        }
        
        
        self.vitality = vitality
        self.stamina = stamina
        self.maxvitality = maxvitality
        self.maxstamina = maxstamina
        self.animationIterator = 0
        self.moving = false
        self.intersects = false
        self.playingSound = false
        self.attackAnimIterator = 0
        self.numberOfItemSlots = 3
        self.numberOfItems = 0
        hero = SKSpriteNode(texture: heroTexturesU[0])
        direction = "UP"
    }
    
}
