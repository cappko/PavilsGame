//
//  GameScene.swift
//  PavilsGame
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import SpriteKit
import GameplayKit
import AVFoundation
import Darwin

//DAY AND NIGHT CYCLE
var time: Double = 0.0
var switchedNoon: Bool = false
let questHandler = QuestHandler()
var pavils = Hero(vitality: 100, stamina: 100, maxvitality: 100, maxstamina: 100)

class GameScene: SKScene, SKPhysicsContactDelegate
{
    let worldType = "GameScene"
    
    //CHARACTERS INITIALIZATION
    
    let playerCategory: UInt32 = 0x1 << 0
    
    //TOUCH POINTS
    var touch: UITouch? = nil
    var touchLocation: CGPoint? = nil
    var previousLocation: CGPoint? = nil
    var pressedControls = [SKSpriteNode: UITouch]()
    var playerDirection = "front"
    var inAttack: Bool = false
    
    //SPRITE NODES DEFINITION
    var worldA1 = SKSpriteNode()
    var worldA2 = SKSpriteNode()
    var worldB1 = SKSpriteNode()
    var worldB2 = SKSpriteNode()
    var worldC1 = SKSpriteNode()
    var worldC2 = SKSpriteNode()
    var worldD1 = SKSpriteNode()
    var worldD2 = SKSpriteNode()
    var sprintButton = SKSpriteNode()
    var actionButton = SKSpriteNode()
    var attackButton = SKSpriteNode()
    var pickUpButton = SKSpriteNode()
    var trees: [SKSpriteNode] = []
    
    //house -30 -204
    
    //VEGETATION
    var treesX: [CGFloat] = [-3150, -4502, -1641, -2561, -3518, 227, -3870, -2130, -3021, -684, -201, -4691, 743, -1774, 1007, -2326, -806, -1131, -3287, -1451, -4358, -3520, 165, -2557, -4025, -3359, -4286, -1987, 237, -2683, -446, -3730, -911, -3193, 81, 438, 771, -4619, 1308, 1621, -1025, -2737, -1313, -3970, -2911, 1473, 735, 328, -2491, 12, -3232, -2311, -359, -1004, -4012, -622, -247, 127, 531, 1019, -3611, -3416, 1328, -1131, -2824, -1743, -1022, -1509, -672, 1449, -1872, 1664, 819, 174, -338, -874, -187, 309, 723, 1186, 1611, 1425, 1117, 865, 631, 439, 154, -191, -508, -1162, -2460, 94, 1058, -351]
    var treesY: [CGFloat] = [7450, 7371, 7280, 7280, 7147, 6944, 6900, 6910, 6903, 6881, 6704, 6818, 6818, 6802, 6452, 6160, 6656, 4227, 4007, 3962, 3822, 3800, 3680, 3642, 3228, 3195, 3194, 3112, 3003, 2809, 2808, 2664, 2580, 2559, 2406, 2338, 2280, 2411, 2400, 2310, 2298, 2250, 2235, 2112, 2082, 2056, 1987, 1906, 1938, 1813, 1812, 1806, 1802, 1782, 1617, 1534, 1501, 1408, 1390, 1536, 1304, 1166, 1083, 1191, 951, 947, 654, 521, 500, 564, 50, 1552, 664, 884, 800, 1028, 203, 154, 226, 82, -36, -219, -120, 25, -195, 35, -145, 46, -114, -284, -285, 700, 2560, 2243]
    var treeCollisions: [SKShapeNode] = []
    let treeCategory: UInt32 = 0x1 << 0
    
    //STRUCTURES
    var playerHouseTexture = SKTexture(imageNamed: "hut.png")
    var playerHouse = SKSpriteNode()
    var houseCollision = SKShapeNode()
    var townBuilding1 = SKSpriteNode()
    var townBuilding2 = SKSpriteNode()
    var townBuilding3 = SKSpriteNode()
    var townBuilding4 = SKSpriteNode()
    var townBuilding5 = SKSpriteNode()
    var townBuilding6 = SKSpriteNode()
    var townWell = SKSpriteNode()
    
    var millTextures: [SKTexture] = []
    var millWheel = SKSpriteNode()
    var millWheelIterator: Int = 0
    var mill = SKSpriteNode()
    
    //UI NODES
    let nameHUDLabel = SKLabelNode(fontNamed: "Georgia")
    var vitalHUD = SKSpriteNode()
    var vitalityBar = SKSpriteNode(imageNamed: "lifebar.png")
    var staminaBar = SKSpriteNode(imageNamed: "staminabar.png")
    var overviewHud = SKSpriteNode()
    var timeHud = SKLabelNode()
    
    //ANALOG
    var analog = SKSpriteNode()
    var analogUp = SKShapeNode()
    var analogDown = SKShapeNode()
    var analogLeft = SKShapeNode()
    var analogRight = SKShapeNode()
    var analogDefault = SKShapeNode()
    
    //LIGHTING
    var screenScale: CGFloat = 1.0
    var screenH: CGFloat = 640.0
    var screenW: CGFloat = 960.0
    
    var lightSprite: SKSpriteNode?
    var ambientColor = UIColor.darkGray
    
    //TEXTURES
    
    //UI TEXTURES
    var analogTexture = SKTexture(imageNamed: "analog.png")
    var sprintTexture = SKTexture(imageNamed: "sprint.png")
    var actionTexture = SKTexture(imageNamed: "actionButton.png")
    var attackTexture = SKTexture(imageNamed: "attackButton.png")
    var pickUpTexture = SKTexture(imageNamed: "pickUpButton.png")
    var sprintPressedTexture = SKTexture(imageNamed: "sprintPressed.png")
    var vitalHUDTexture = SKTexture(imageNamed: "HUDparchment.png")
    
    //MENU
    var ingameMenu: IngameMenu?
    var screenshot: UIImage?
    
    //MAP
    var gameMap = SKSpriteNode(imageNamed: "gameMap.png")
    var gameMarkers: [SKNode] = []
    var gameMarker = SKShapeNode(circleOfRadius: 3)
    var playerHouseMarker = SKSpriteNode(imageNamed: "hut.png")
    var millHouseMarker = SKSpriteNode(imageNamed: "mill.png")
    var millWheelMarker = SKSpriteNode(imageNamed: "millsprite0.png")
    var townBuilding1Marker = SKSpriteNode(imageNamed: "townBuilding1.png")
    var townBuilding2Marker = SKSpriteNode(imageNamed: "townBuilding2.png")
    var townBuilding3Marker = SKSpriteNode(imageNamed: "townBuilding3.png")
    var townBuilding4Marker = SKSpriteNode(imageNamed: "townBuilding4.png")
    var townBuilding5Marker = SKSpriteNode(imageNamed: "townBuilding5.png")
    var townBuilding6Marker = SKSpriteNode(imageNamed: "townBuilding6.png")
    
    //WATER SURFACES
    var waterSurfaceIterator: Int = 0
    var waterTextures: [SKTexture] = []
    var waterSurfaces: [SKSpriteNode] = []
    
    //WORLD TEXTURES
    var worldTextures: [SKTexture] = []
    
    //VEGETATION TEXTURES
    var treeTextures: [SKTexture] = []
    
    //CAMERA
    var cam: SKCameraNode!
    
    var overlayView: SKShapeNode!
    
    //SWITCHES
    var toMenu: Bool = false
    var inMenu: Bool = false

    //FX
    var videoPlayer = SKVideoNode()
    var skipButton = SKSpriteNode()
    
    //ACTION AREAS
    var actionAreas: [SKShapeNode] = []
    
    override func didMove(to view: SKView)
    {
        ingameMenu = IngameMenu(selfViewWidth: (self.view?.frame.width)!, selfViewHeight: (self.view?.frame.height)!)
        self.physicsWorld.contactDelegate = self
        
        //WATER IMPLEMENTATION
        for i in 1..<11
        {
            waterTextures.append(SKTexture(imageNamed: "\(i).png"))
        }
        for _ in 0..<23
        {
            let waterSurface = SKSpriteNode(texture: waterTextures[0])
            waterSurface.zPosition = -1
            waterSurface.size = CGSize(width: 560, height: 300)
            waterSurfaces.append(waterSurface)
        }
        
        waterSurfaces[0].position = CGPoint(x: -2691.48, y: -543.47)
        waterSurfaces[1].position = CGPoint(x: -2574.97, y: -259.471)
        waterSurfaces[2].position = CGPoint(x: -2470.97, y: 25.471)
        waterSurfaces[3].position = CGPoint(x: -2342.97, y: 295.471)
        waterSurfaces[4].position = CGPoint(x: -2185.97, y: 590.471)
        waterSurfaces[5].position = CGPoint(x: -2035.97, y: 890.471)
        waterSurfaces[6].position = CGPoint(x: -1935.97, y: 1190.471)
        waterSurfaces[7].position = CGPoint(x: -1835.97, y: 1490.471)
        waterSurfaces[8].position = CGPoint(x: -1735.97, y: 1790.471)
        waterSurfaces[9].position = CGPoint(x: -1635.97, y: 2090.471)
        waterSurfaces[10].position = CGPoint(x: -1490.97, y: 2390.471)
        waterSurfaces[11].position = CGPoint(x: -1350.97, y: 2690.471)
        waterSurfaces[12].position = CGPoint(x: -1045.97, y: 2990.471)
        waterSurfaces[13].position = CGPoint(x: -785.97, y: 3290.471)
        waterSurfaces[14].position = CGPoint(x: -570.97, y: 3590.471)
        waterSurfaces[15].position = CGPoint(x: -355.97, y: 3890.471)
        waterSurfaces[16].position = CGPoint(x: -155.97, y: 4190.471)
        waterSurfaces[17].position = CGPoint(x: 185.97, y: 4490.471)
        waterSurfaces[18].position = CGPoint(x: 415.97, y: 4750.471)
        waterSurfaces[19].position = CGPoint(x: 975.97, y: 4880.471)
        waterSurfaces[20].position = CGPoint(x: 1535.97, y: 4980.471)
        waterSurfaces[21].position = CGPoint(x: 1805.97, y: 5280.471)
        waterSurfaces[22].position = CGPoint(x: 2025.97, y: 5460.471)
        
        for waterSurface in waterSurfaces
        {
            self.addChild(waterSurface)
        }
        
        //TREES IMPLEMENTATION
        for i in 1..<26
        {
            let treeTexture = SKTexture(imageNamed: "tree\(i).png")
            treeTextures.append(treeTexture)
        }
        
        var treeTextureIterator = 1
        for i in 0..<treesX.count
        {
            if treeTextureIterator > 25
            {
                treeTextureIterator = 1
            }
            let tree = SKSpriteNode(texture: treeTextures[treeTextureIterator])
            tree.position = CGPoint(x: treesX[i], y: treesY[i])
            tree.zPosition = 3
            tree.lightingBitMask = 1
            trees.append(tree)
            treeTextureIterator += 1
            
            var treeCollision = SKShapeNode(rect: CGRect(x: 20, y: 0, width: 20, height: 5))
            treeCollision.name = "bar"
            treeCollision.physicsBody = SKPhysicsBody(rectangleOf: treeCollision.frame.size)
            treeCollision.physicsBody?.usesPreciseCollisionDetection = true
            treeCollision.physicsBody?.affectedByGravity = false
            treeCollision.physicsBody?.isDynamic = false
            treeCollision.physicsBody?.categoryBitMask = treeCategory
            treeCollision.zPosition = 3
            treeCollision.alpha = 0.0
            treeCollision.fillColor = SKColor.blue
            treeCollision.position = tree.position
            if treeTextureIterator - 1 == 1
            {
                treeCollision.position = CGPoint(x: tree.position.x - 18, y: tree.position.y - tree.frame.height / 4 - 8)
            }
            if treeTextureIterator - 1 == 2 || treeTextureIterator - 1 == 3
            {
                treeCollision.position = CGPoint(x: tree.position.x - 10, y: tree.position.y - tree.frame.height / 3 - 8)
            }
            if treeTextureIterator - 1 == 4 || treeTextureIterator - 1 == 5 || treeTextureIterator - 1 == 8 || treeTextureIterator - 1 == 9
            {
                treeCollision.position = CGPoint(x: tree.position.x - 25, y: tree.position.y - tree.frame.height / 3 - 45)
            }
            if treeTextureIterator - 1 == 6 || treeTextureIterator - 1 == 7 || treeTextureIterator - 1 == 10 || treeTextureIterator - 1 == 11
            {
                treeCollision.position = CGPoint(x: tree.position.x - 25, y: tree.position.y - tree.frame.height / 3 - 65)
            }
            if treeTextureIterator - 1 == 12
            {
                treeCollision.position = CGPoint(x: tree.position.x - 25, y: tree.position.y - tree.frame.height / 3 - 70)
            }
            if treeTextureIterator - 1 == 13  || treeTextureIterator - 1 == 14 || treeTextureIterator - 1 == 12 || treeTextureIterator - 1 == 17 || treeTextureIterator - 1 == 18
            {
                treeCollision.position = CGPoint(x: tree.position.x - 25, y: tree.position.y - tree.frame.height / 3 - 50)
            }
            if treeTextureIterator - 1 == 15 || treeTextureIterator - 1 == 16
            {
                treeCollision.position = CGPoint(x: tree.position.x - 25, y: tree.position.y - tree.frame.height / 3 - 40)
            }
            if treeTextureIterator - 1 == 19 || treeTextureIterator - 1 == 20
            {
                treeCollision.position = CGPoint(x: tree.position.x - 25, y: tree.position.y - tree.frame.height / 3 + 40)
            }
            
            treeCollisions.append(treeCollision)
            
            if treeTextureIterator + 1 == 21
            {
                treeTextureIterator = 1
            }
        }

        for i in 0..<trees.count
        {
            self.addChild(trees[i])
            self.addChild(treeCollisions[i])
        }
        
        //WORLD TEXTURES LOADING
        let worldA1Texture = SKTexture(imageNamed: "worldA1.png")
        let worldA2Texture = SKTexture(imageNamed: "worldA2.png")
        let worldB1Texture = SKTexture(imageNamed: "worldB1.png")
        let worldB2Texture = SKTexture(imageNamed: "worldB2.png")
        let worldC1Texture = SKTexture(imageNamed: "worldC1.png")
        let worldC2Texture = SKTexture(imageNamed: "worldC2.png")
        let worldD1Texture = SKTexture(imageNamed: "worldD1.png")
        let worldD2Texture = SKTexture(imageNamed: "worldD2.png")
        
        //LIGHTING
        screenH = view.frame.height
        screenW = view.frame.width
        screenScale = screenW / 1920
        
        ambientColor = UIColor.darkGray
        lightSprite = SKSpriteNode()
        lightSprite?.setScale(screenScale * 2)
        lightSprite?.position = CGPoint(x: screenW - 100, y: screenH - 100)
        //self.addChild(lightSprite!)
        
        let light = SKLightNode()
        light.position = CGPoint(x: 0, y: 0)
        light.falloff = 2
        light.ambientColor = ambientColor
        light.lightColor = UIColor.black
        //pavils.hero.addChild(light)

        //SPRITE NODE TEXTURES IMPLEMENTATION
        worldA1 = SKSpriteNode(texture: worldA1Texture)
        worldA1.lightingBitMask = 1
        worldA1.position = CGPoint(x: self.frame.width / 2 - 3840.0, y: self.frame.height / 2 + 6480.0)
        self.addChild(worldA1)
        
        worldA2 = SKSpriteNode(texture: worldA2Texture)
        worldA2.lightingBitMask = 1
        worldA2.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 + 6480.0)
        self.addChild(worldA2)
        
        worldB1 = SKSpriteNode(texture: worldB1Texture)
        worldB1.lightingBitMask = 1
        worldB1.position = CGPoint(x: self.frame.width / 2 - 3840.0, y: self.frame.height / 2 + 4320.0)
        self.addChild(worldB1)
        
        worldB2 = SKSpriteNode(texture: worldB2Texture)
        worldB2.lightingBitMask = 1
        worldB2.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 + 4320.0)
        self.addChild(worldB2)
        
        worldC1 = SKSpriteNode(texture: worldC1Texture)
        worldC1.lightingBitMask = 1
        worldC1.position = CGPoint(x: self.frame.width / 2 - 3840.0, y: self.frame.height / 2 + 2160.0)
        self.addChild(worldC1)
        
        worldC2 = SKSpriteNode(texture: worldC2Texture)
        worldC2.lightingBitMask = 1
        worldC2.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2 + 2160.0)
        self.addChild(worldC2)
        
        worldD1 = SKSpriteNode(texture: worldD1Texture)
        worldD1.lightingBitMask = 1
        worldD1.position = CGPoint(x: self.frame.width / 2 - 3840.0, y: self.frame.height / 2)
        self.addChild(worldD1)
        
        worldD2 = SKSpriteNode(texture: worldD2Texture)
        worldD2.lightingBitMask = 1
        worldD2.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        self.addChild(worldD2)
   
        
        //STRUCTURES
        playerHouse = SKSpriteNode(texture: playerHouseTexture)
        playerHouse.lightingBitMask = 1
        playerHouse.position = CGPoint(x: -592, y: 965)
        playerHouse.zPosition = 2
        playerHouse.physicsBody = SKPhysicsBody(texture: playerHouseTexture, size: playerHouseTexture.size())
        playerHouse.physicsBody?.usesPreciseCollisionDetection = true
        playerHouse.physicsBody?.affectedByGravity = false
        playerHouse.physicsBody?.isDynamic = false
        playerHouse.physicsBody?.categoryBitMask = treeCategory
        playerHouse.physicsBody?.allowsRotation = false
        self.addChild(playerHouse)
        
        houseCollision = SKShapeNode(rect: CGRect(x: 20, y: 0, width: 40, height: 5))
        houseCollision.zPosition = 2
        houseCollision.alpha = 0.0
        houseCollision.fillColor = SKColor.blue
        houseCollision.position = CGPoint(x: -375, y: 763)
        self.addChild(houseCollision)
        actionAreas.append(houseCollision)
        
        for i in (0...4).reversed()
        {
            let millWheelTexture = SKTexture(imageNamed: "millsprite\(i).png")
            millTextures.append(millWheelTexture)
        }
        
        mill = SKSpriteNode(texture: SKTexture(imageNamed: "mill.png"))
        mill.position = CGPoint(x: -100.0175, y: 4900.4)
        mill.zPosition = 2
        mill.isHidden = false
        mill.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "mill.png"), size: SKTexture(imageNamed: "mill.png").size())
        mill.physicsBody?.usesPreciseCollisionDetection = true
        mill.physicsBody?.affectedByGravity = false
        mill.physicsBody?.isDynamic = false
        mill.physicsBody?.categoryBitMask = treeCategory
        mill.physicsBody?.allowsRotation = false
        self.addChild(mill)
        
        millWheel = SKSpriteNode(texture: millTextures[0])
        millWheel.position = CGPoint(x: 282.0175, y: 4724.4)
        millWheel.zPosition = 3
        millWheel.isHidden = false
        millWheel.physicsBody = SKPhysicsBody(texture: millTextures[0], size: millTextures[0].size())
        millWheel.physicsBody?.usesPreciseCollisionDetection = true
        millWheel.physicsBody?.affectedByGravity = false
        millWheel.physicsBody?.isDynamic = false
        millWheel.physicsBody?.categoryBitMask = treeCategory
        millWheel.physicsBody?.allowsRotation = false
        self.addChild(millWheel)
        
        var townBuildings: [SKSpriteNode] = []
        
        townBuildings.append(townBuilding1)
        townBuildings.append(townBuilding2)
        townBuildings.append(townBuilding3)
        townBuildings.append(townBuilding4)
        townBuildings.append(townBuilding5)
        townBuildings.append(townBuilding6)
        
        townWell = SKSpriteNode(imageNamed: "well.png")
        townWell.position = CGPoint(x: -3888.74, y: 5510.81)
        townWell.zPosition = 3
        townWell.isHidden = false
        townWell.physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "well.png"), size: SKTexture(imageNamed: "well.png").size())
        townWell.physicsBody?.usesPreciseCollisionDetection = true
        townWell.physicsBody?.affectedByGravity = false
        townWell.physicsBody?.isDynamic = false
        townWell.physicsBody?.categoryBitMask = treeCategory
        townWell.physicsBody?.allowsRotation = false
        self.addChild(townWell)
        
        for i in 0..<townBuildings.count
        {
            townBuildings[i].texture = SKTexture(imageNamed: "townBuilding\(i+1).png")
            townBuildings[i].zPosition = 3
            if i == 1
            {
                townBuildings[i].physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "townBuilding\(i+1).png"), size: CGSize(width: SKTexture(imageNamed: "townBuilding\(i+1).png").size().width / 1.5, height: SKTexture(imageNamed: "townBuilding\(i+1).png").size().height / 1.5))
            }
            else
            {
                townBuildings[i].physicsBody = SKPhysicsBody(texture: SKTexture(imageNamed: "townBuilding\(i+1).png"), size: SKTexture(imageNamed: "townBuilding\(i+1).png").size())
            }
            townBuildings[i].physicsBody?.usesPreciseCollisionDetection = true
            townBuildings[i].physicsBody?.affectedByGravity = false
            townBuildings[i].physicsBody?.isDynamic = false
            townBuildings[i].physicsBody?.categoryBitMask = treeCategory
            townBuildings[i].physicsBody?.allowsRotation = false
            townBuildings[i].size = SKTexture(imageNamed: "townBuilding\(i+1).png").size()
            self.addChild(townBuildings[i])
            print(i)
        }

        townBuilding1.position = CGPoint(x: -3309.56, y: 5905.56)
        
        townBuilding2.size = CGSize(width: SKTexture(imageNamed: "townBuilding2.png").size().width / 1.5, height: SKTexture(imageNamed: "townBuilding2.png").size().height / 1.5)
        townBuilding2.position = CGPoint(x: -4724.54, y: 5334.21)
        
        townBuilding3.position = CGPoint(x: -4123.55, y: 6177.36)
        townBuilding4.position = CGPoint(x: -3817.49, y: 4808.93)
        townBuilding5.position = CGPoint(x: -2590.01, y: 5129.7)
        townBuilding6.position = CGPoint(x: -2527.59, y: 4334.17)
        
        //pavils.hero.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        pavils.hero.position = CGPoint(x: (loadedGame?.playerPositionX)!, y: (loadedGame?.playerPositionY)!)
        //pavils.items = (loadedGame?.items)!
        //ingameMenu?.items = (loadedGame?.inventoryItems)!
        pavils.hero.zPosition = 2
        pavils.hero.physicsBody = SKPhysicsBody(rectangleOf: pavils.hero.size)
        pavils.hero.physicsBody?.affectedByGravity = false
        pavils.hero.physicsBody?.isDynamic = true
        pavils.hero.physicsBody?.allowsRotation = false
        pavils.hero.physicsBody?.usesPreciseCollisionDetection = true
        pavils.hero.physicsBody?.categoryBitMask = playerCategory
        pavils.hero.lightingBitMask = 1
        
        if (pavils.hero.parent != nil)
        {
            pavils.hero.removeFromParent()
        }
        self.addChild(pavils.hero)
        
        
        //UI RENDERING
        cam = SKCameraNode()
        cam.xScale = 1.0
        cam.yScale = 1.0
        self.camera = cam
        self.addChild(cam)
        cam.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        
        if newGame == true
        {
            
            videoPlayer = SKVideoNode(fileNamed: "Intro.mp4")// SKVideoNode(URL: fileUrl!)
            videoPlayer.zPosition = 8
            videoPlayer.size = CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!)
            //videoPlayer.frame = CGRect(x: 0, y: 0, width: self.view?.frame.width, height: self.view?.frame.height)
            videoPlayer.position = CGPoint(x: 0, y: 0)
            cam.addChild(videoPlayer)
            videoPlayer.play()
            skipButton.texture = sprintTexture
            skipButton.zPosition = 9
            skipButton.size = CGSize(width: 30, height: 30)
            skipButton.position = convert(CGPoint(x: 70, y: self.size.height - 52), to: cam)
            cam.addChild(skipButton)
            pavils.hero.position = CGPoint(x: 1430.97, y: 195.541)
            
            questHandler.gameQuestLabel.text = "TUTORIAL: Movement: analog stick. Visit your first quest (blue marker)"
            questHandler.quest1Marker.position = CGPoint(x: 1489.93, y: 1343.9)
            self.addChild(questHandler.quest1Marker)
            questHandler.quest1MapMarker.position = CGPoint(x: (((questHandler.quest1Marker.position.x + 4722) / 39) - 84), y: (((questHandler.quest1Marker.position.y + 283) / 41) - 94.5))
            gameMap.addChild(questHandler.quest1MapMarker)
            questHandler.activeQuest = 0
            
            
        }
        
        for item in pavils.items
        {
            if item.itemInInventory == true
            {
                item.itemObject.size = CGSize(width: 80, height: 80)
                item.itemObject.isHidden = true
            }
            self.addChild(item.itemObject)
            
        }
        
        for i in 0..<Int((ingameMenu?.items.count)!)
        {
            ingameMenu?.items[i].itemObject.position = CGPoint(x: (ingameMenu?.itemSlots[i].position.x)! + (ingameMenu?.itemSlots[i].frame.width)! / 2, y: (ingameMenu?.itemSlots[i].position.y)! + (ingameMenu?.itemSlots[i].frame.height)! / 2)
            
            ingameMenu?.items[i].itemObject.isHidden = false
            if ingameMenu?.items[i].itemObject.parent != cam
            {
                cam.addChild((ingameMenu?.items[i].itemObject)!)
            }
            
            ingameMenu?.items[i].itemObject.isHidden = true
        }
        
        let analogHeight: CGFloat = 50.0
        analogDefault = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogDefault.fillColor = UIColor.red
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            analogDefault.position = convert(CGPoint(x: self.frame.width / 7 - analogHeight / 2, y: self.frame.midY / 2 - analogHeight / 2), to: cam)
        }
        else if UIDevice.current.userInterfaceIdiom == .pad
        {
            analogDefault.position = convert(CGPoint(x: self.frame.width / 9 - analogHeight / 2, y: self.frame.midY / 3 - analogHeight / 2), to: cam)
        }
        analogDefault.zPosition = 4
        analogDefault.isHidden = true
        cam.addChild(analogDefault)
        
        //ANALOG RENDERING
        analog = SKSpriteNode(texture: analogTexture)
        analog.position = CGPoint(x: analogDefault.position.x + analogHeight / 2, y: analogDefault.position.y + analogHeight / 2)
        analog.zPosition = 7
        cam.addChild(analog)
        
        let analogPosition = CGPoint(x: analog.position.x, y: analog.position.y)
        
        analogUp = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogUp.fillColor = UIColor.green
        analogUp.position = (CGPoint(x: analogPosition.x - analogHeight / 2, y: analogPosition.y + analogHeight / 2 + 10))
        analogUp.zPosition = 4
        analogUp.isHidden = true
        cam.addChild(analogUp)
        
        
        analogDown = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogDown.fillColor = UIColor.green
        analogDown.position = (CGPoint(x: analogPosition.x - analogHeight / 2, y: analogPosition.y - analogHeight * 1.5 - 10))
        analogDown.zPosition = 4
        analogDown.isHidden = true
        cam.addChild(analogDown)
        
        
        analogLeft = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogLeft.fillColor = UIColor.green
        analogLeft.position = (CGPoint(x: analogPosition.x - analogHeight * 1.5 - 10, y: analogPosition.y - analogHeight / 2))
        analogLeft.zPosition = 4
        analogLeft.isHidden = true
        cam.addChild(analogLeft)
        
        
        analogRight = SKShapeNode(rect: CGRect(x: 0, y: 0, width: analogHeight, height: analogHeight))
        analogRight.fillColor = UIColor.green
        analogRight.position = (CGPoint(x: analogPosition.x + analogHeight / 2 + 10, y: analogPosition.y - analogHeight / 2))
        analogRight.zPosition = 4
        analogRight.isHidden = true
        cam.addChild(analogRight)
        
        
        sprintButton = SKSpriteNode(texture: sprintTexture)
        sprintButton.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.frame.midY / 3), to: cam)
        sprintButton.zPosition = 7
        cam.addChild(sprintButton)
        
        actionButton = SKSpriteNode(texture: actionTexture)
        actionButton.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.frame.midY / 1.35), to: cam)
        actionButton.zPosition = 7
        actionButton.isHidden = true
        cam.addChild(actionButton)
        
        attackButton = SKSpriteNode(texture: attackTexture)
        attackButton.position = convert(CGPoint(x: self.frame.width / 1.3, y: self.frame.midY / 3), to: cam)
        attackButton.zPosition = 7
        cam.addChild(attackButton)
        
        pickUpButton = SKSpriteNode(texture: pickUpTexture)
        pickUpButton.position = convert(CGPoint(x: self.frame.width / 1.3, y: self.frame.midY / 1.35), to: cam)
        pickUpButton.zPosition = 7
        cam.addChild(pickUpButton)
        pickUpButton.isHidden = true
        
        vitalHUD = SKSpriteNode(texture: vitalHUDTexture)
        vitalHUD.position = convert(CGPoint(x: 70, y: self.size.height - 50), to: cam)
        vitalHUD.zPosition = 7
        cam.addChild(vitalHUD)
        
        nameHUDLabel.text = "Pavils"
        nameHUDLabel.fontSize = 20
        nameHUDLabel.zPosition = 8
        nameHUDLabel.fontColor = UIColor.black
        nameHUDLabel.position = convert(CGPoint(x: 45, y: self.size.height - 35), to: cam)
        cam.addChild(nameHUDLabel)
        
        vitalityBar.size = CGSize(width: pavils.vitality, height: 10.0)
        vitalityBar.position = convert(CGPoint(x: 70, y: self.size.height - 52), to: cam)
        vitalityBar.zPosition = 8
        cam.addChild(vitalityBar)
        
        staminaBar.size = CGSize(width: pavils.stamina, height: 10.0)
        staminaBar.position = convert(CGPoint(x: 70, y: self.size.height - 70), to: cam)
        staminaBar.zPosition = 8
        cam.addChild(staminaBar)
        
        //MENU
        
        if UIDevice.current.userInterfaceIdiom == .phone || UIDevice.current.userInterfaceIdiom == .pad
        {
            ingameMenu?.menuBorder.position = convert(CGPoint(x: 100, y: 40), to: cam)
            ingameMenu?.menuButton1.position = convert(CGPoint(x: 100, y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            ingameMenu?.menuButton2.position = convert(CGPoint(x: (100 + ((ingameMenu?.selfViewWidth)! - 220) / 3), y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            ingameMenu?.menuButton3.position = convert(CGPoint(x: (100 + (2 * ((ingameMenu?.selfViewWidth)! - 220) / 3)), y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            
            ingameMenu?.exitButtonX.position = convert(CGPoint(x: (100 + (2.7 * ((ingameMenu?.selfViewWidth)! - 100) / 3)), y: (ingameMenu?.menuBorder.frame.height)!), to: cam)
            
            ingameMenu?.menuButton1Text.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! + 10)
            ingameMenu?.menuButton2Text.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + ((ingameMenu?.menuButton2.frame.width)! / 2), y: (ingameMenu?.menuButton2.position.y)! + 10)
            ingameMenu?.menuButton3Text.position = CGPoint(x: (ingameMenu?.menuButton3.position.x)! + ((ingameMenu?.menuButton3.frame.width)! / 2), y: (ingameMenu?.menuButton3.position.y)! + 10)
            
            ingameMenu?.gameMenuSaveButtonText.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! - 50)
            ingameMenu?.gameMenuLoadButtonText.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! - 150)
            ingameMenu?.gameMenuExitButtonText.position = CGPoint(x: (ingameMenu?.menuButton1.position.x)! + ((ingameMenu?.menuButton1.frame.width)! / 2), y: (ingameMenu?.menuButton1.position.y)! - 250)
            
            ingameMenu?.savePosition1.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 85)
            ingameMenu?.savePosition2.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 175)
            ingameMenu?.savePosition3.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 265)
            
            ingameMenu?.savePosition1Image.position = CGPoint(x: (ingameMenu?.savePosition1.position.x)! + (ingameMenu?.savePosition1.frame.width)! / 2, y: (ingameMenu?.savePosition1.position.y)! + ((ingameMenu?.savePosition1.frame.height)! / 2))
            ingameMenu?.savePosition2Image.position = CGPoint(x: (ingameMenu?.savePosition2.position.x)! + (ingameMenu?.savePosition2.frame.width)! / 2, y: (ingameMenu?.savePosition2.position.y)! + ((ingameMenu?.savePosition2.frame.height)! / 2))
            ingameMenu?.savePosition3Image.position = CGPoint(x: (ingameMenu?.savePosition3.position.x)! + (ingameMenu?.savePosition3.frame.width)! / 2, y: (ingameMenu?.savePosition3.position.y)! + ((ingameMenu?.savePosition3.frame.height)! / 2))
            
            ingameMenu?.loadPosition1.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 85)
            ingameMenu?.loadPosition2.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 175)
            ingameMenu?.loadPosition3.position = CGPoint(x: (ingameMenu?.menuButton2.position.x)! + (ingameMenu?.menuButton2.frame.width)! / 2, y: (ingameMenu?.menuButton3.position.y)! - 265)
            
            ingameMenu?.loadPosition1Image.position = CGPoint(x: (ingameMenu?.loadPosition1.position.x)! + (ingameMenu?.loadPosition1.frame.width)! / 2, y: (ingameMenu?.loadPosition1.position.y)! + ((ingameMenu?.loadPosition1.frame.height)! / 2))
            ingameMenu?.loadPosition2Image.position = CGPoint(x: (ingameMenu?.loadPosition2.position.x)! + (ingameMenu?.loadPosition2.frame.width)! / 2, y: (ingameMenu?.loadPosition2.position.y)! + ((ingameMenu?.loadPosition2.frame.height)! / 2))
            ingameMenu?.loadPosition3Image.position = CGPoint(x: (ingameMenu?.loadPosition3.position.x)! + (ingameMenu?.loadPosition3.frame.width)! / 2, y: (ingameMenu?.loadPosition3.position.y)! + ((ingameMenu?.loadPosition3.frame.height)! / 2))
            
            ingameMenu?.pavilsName.position = CGPoint(x: (ingameMenu?.menuButton1Text.position.x)!, y: (ingameMenu?.menuButton1Text.position.y)! - 100)
            ingameMenu?.pavilsCharacter.position = CGPoint(x: (ingameMenu?.pavilsName.position.x)!, y: (ingameMenu?.pavilsName.position.y)! - 100)
            
            ingameMenu?.topAttireName.position = CGPoint(x: (ingameMenu?.menuButton2Text.position.x)!, y: (ingameMenu?.menuButton2Text.position.y)! - 100)
            ingameMenu?.topAttireSlot.position = CGPoint(x: (ingameMenu?.topAttireName.position.x)! - ((ingameMenu?.topAttireName.frame.width)! / 2), y: (ingameMenu?.topAttireName.position.y)! - 100)
            
            ingameMenu?.bottomAttireName.position = CGPoint(x: (ingameMenu?.menuButton3Text.position.x)!, y: (ingameMenu?.menuButton3Text.position.y)! - 100)
            ingameMenu?.bottomAttireSlot.position = CGPoint(x: (ingameMenu?.bottomAttireName.position.x)! - ((ingameMenu?.bottomAttireName.frame.width)! / 2.5), y: (ingameMenu?.bottomAttireName.position.y)! - 100)
            
            ingameMenu?.pavilsArmor.position = CGPoint(x: (ingameMenu?.topAttireSlot.position.x)! + (ingameMenu?.topAttireSlot.frame.width)! / 2, y: (ingameMenu?.topAttireSlot.position.y)! + (ingameMenu?.topAttireSlot.frame.height)! / 2)

            ingameMenu?.pavilsGraves.position = CGPoint(x: (ingameMenu?.bottomAttireSlot.position.x)! + (ingameMenu?.bottomAttireSlot.frame.width)! / 2, y: (ingameMenu?.bottomAttireSlot.position.y)! + (ingameMenu?.bottomAttireSlot.frame.height)! / 2)

            ingameMenu?.itemsName.position = CGPoint(x: (ingameMenu?.menuButton2Text.position.x)!, y: (ingameMenu?.topAttireSlot.position.y)! - 150)
            
            for i in 0..<Int((ingameMenu?.itemSlots.count)!)
            {
                ingameMenu?.itemSlots[i].position = CGPoint(x: ((ingameMenu?.pavilsName.position.x)! + (CGFloat(i) * 100.0)), y: (ingameMenu?.topAttireSlot.position.y)! - 300)
                if i >= pavils.numberOfItemSlots
                {
                    ingameMenu?.itemSlots[i].strokeColor = UIColor.gray
                    ingameMenu?.itemSlots[i].fillColor = UIColor.gray
                }
            }
            
            questHandler.gameQuestLabel.position = CGPoint(x: vitalityBar.position.x + 420, y: vitalityBar.position.y)
            if questHandler.gameQuestLabel.parent != nil
            {
                questHandler.gameQuestLabel.removeFromParent()
            }
            cam.addChild(questHandler.gameQuestLabel)
            
            if exitingHouse == true
            {
                pavils.hero.position = houseCollision.position
                exitingHouse = false
            }
            
        }
        /*else if UIDevice.current.userInterfaceIdiom == .pad
        {
            ingameMenu?.menuBorder.position = convert(CGPoint(x: 150, y: 25), to: cam)
        }*/
        
        for component in (ingameMenu?.menuComponents)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.menu2Components)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.menu3Components)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.saveComponents)!
        {
            component.isHidden = true
            cam.addChild(component)
        }
        
        for component in (ingameMenu?.loadComponents)!
        {
            component.isHidden = true
            cam.addChild(component)
        }

        
        /*//BLUR
        let duration: CGFloat = 0.5
        let filter: CIFilter = CIFilter(name: "CIGaussianBlur", withInputParameters: ["inputRadius" : NSNumber(value:1.0)])!
        scene!.filter = filter
        scene!.shouldRasterize = true
        scene!.shouldEnableEffects = true
        scene!.run(SKAction.customAction(withDuration: 0.5, actionBlock: { (node: SKNode, elapsedTime: CGFloat) in
            let radius = (elapsedTime/duration)*10.0
            (node as? SKEffectNode)!.filter!.setValue(radius, forKey: "inputRadius")
            
        }))*/

        //if newGame == true
        //{
        
        
        //}
        
        overviewHud = SKSpriteNode(texture: vitalHUDTexture)
        overviewHud.position = convert(CGPoint(x: self.frame.width / 1.15, y: self.size.height - 50), to: cam)
        overviewHud.zPosition = 7
        cam.addChild(overviewHud)
        
        timeHud = SKLabelNode(fontNamed: "Georgia")
        timeHud.fontColor = UIColor.black
        timeHud.fontSize = 30
        timeHud.zPosition = 8
        timeHud.position = CGPoint(x: overviewHud.position.x, y: overviewHud.position.y)
        timeHud.text = "12 AM"
        cam.addChild(timeHud)
        
        gameMap.zPosition = 7
        gameMarker.zPosition = 9
        gameMarker.fillColor = UIColor.yellow
        
        playerHouseMarker.zPosition = 8
        playerHouseMarker.size = CGSize(width: 30, height: 24)
        
        millHouseMarker.zPosition = 8
        millHouseMarker.size = CGSize(width: 30, height: 24)
        millWheelMarker.zPosition = 8
        millWheelMarker.size = CGSize(width: 15, height: 12)
    
        
        gameMap.size = CGSize(width: 168.3, height: 189.3)
        gameMap.position = CGPoint(x: timeHud.position.x, y: timeHud.position.y - 155)
        cam.addChild(gameMap)
        gameMap.addChild(gameMarker)
        gameMap.addChild(playerHouseMarker)
        gameMap.addChild(millHouseMarker)
        gameMap.addChild(millWheelMarker)
        
        townBuilding1Marker.zPosition = 8
        townBuilding2Marker.zPosition = 8
        townBuilding3Marker.zPosition = 8
        townBuilding4Marker.zPosition = 8
        townBuilding5Marker.zPosition = 8
        townBuilding6Marker.zPosition = 8
        
        gameMap.addChild(townBuilding1Marker)
        gameMap.addChild(townBuilding2Marker)
        gameMap.addChild(townBuilding3Marker)
        gameMap.addChild(townBuilding4Marker)
        gameMap.addChild(townBuilding5Marker)
        gameMap.addChild(townBuilding6Marker)
        
        playerHouseMarker.position = CGPoint(x: (((-884.236 + 4722) / 39) - 84), y: (((1342.85 + 283) / 41) - 94.5))
        millHouseMarker.position = CGPoint(x: (((-100.0175 + 4722) / 39) - 84), y: (((4900.4 + 283) / 41) - 94.5))
        millWheelMarker.position = CGPoint(x: (((282.0175 + 4722) / 39) - 84), y: (((4724.4 + 283) / 41) - 94.5))
        
        townBuilding1Marker.position = CGPoint(x: (((4800 + townBuilding1.position.x) / 39) - 84), y: (((283 + townBuilding1.position.y) / 41) - 94.5))
        townBuilding2Marker.position = CGPoint(x: (((4900 + townBuilding2.position.x) / 39) - 84), y: (((283 + townBuilding2.position.y) / 41) - 94.5))
        townBuilding3Marker.position = CGPoint(x: (((4800 + townBuilding3.position.x) / 39) - 84), y: (((283 + townBuilding3.position.y) / 41) - 94.5))
        townBuilding4Marker.position = CGPoint(x: (((4800 + townBuilding4.position.x) / 39) - 84), y: (((283 + townBuilding4.position.y) / 41) - 94.5))
        townBuilding5Marker.position = CGPoint(x: (((4800 + townBuilding5.position.x) / 39) - 84), y: (((283 + townBuilding5.position.y) / 41) - 94.5))
        townBuilding6Marker.position = CGPoint(x: (((4800 + townBuilding6.position.x) / 39) - 84), y: (((283 + townBuilding6.position.y) / 41) - 94.5))
        
        townBuilding1Marker.size = CGSize(width: 30, height: 24)
        townBuilding2Marker.size = CGSize(width: 30, height: 24)
        townBuilding3Marker.size = CGSize(width: 30, height: 24)
        townBuilding4Marker.size = CGSize(width: 30, height: 24)
        townBuilding5Marker.size = CGSize(width: 30, height: 24)
        townBuilding6Marker.size = CGSize(width: 30, height: 24)
        
        gameMarkers.append(gameMarker)
        gameMarkers.append(playerHouseMarker)
        gameMarkers.append(millHouseMarker)
        gameMarkers.append(millWheelMarker)
        gameMarkers.append(townBuilding1Marker)
        gameMarkers.append(townBuilding2Marker)
        gameMarkers.append(townBuilding3Marker)
        gameMarkers.append(townBuilding4Marker)
        gameMarkers.append(townBuilding5Marker)
        gameMarkers.append(townBuilding6Marker)
        
        overlayView = SKShapeNode(rectOf: CGSize(width: (self.view?.frame.width)!, height: (self.view?.frame.height)!))
        overlayView.position = CGPoint(x: 0, y: 0)
        overlayView.zPosition = 6
        overlayView.strokeColor = UIColor.black
        overlayView.fillColor = UIColor.black
        overlayView.alpha = 0.8
        //cam.addChild(overlayView)
        
        /*overlayView = UIView(frame: CGRect(x: 0, y: 0, width: self.view!.frame.width, height: (self.view?.frame.height)!))
        overlayView.backgroundColor = UIColor.black
        overlayView.alpha = 0.0
        overlayView.center = CGPoint(x: (self.view?.frame.width)! / 2, y: (self.view?.frame.height)! / 2)
        self.view?.addSubview(overlayView)
        overlayView.isHidden = true*/
        
        //DAY AND NIGHT CYCLE
        _ = Timer.scheduledTimer(timeInterval: 12, target: self, selector: #selector(GameScene.dayAndNightHandler), userInfo: nil, repeats: true)
        
        if newGame == true
        {
            for marker in gameMarkers
            {
                marker.isHidden = true
            }
            
            timeHud.isHidden = true
            nameHUDLabel.isHidden = true
            vitalHUD.isHidden = true
            vitalityBar.isHidden = true
            staminaBar.isHidden = true
        }
        else
        {
            SKTAudio.sharedInstance().playBackgroundMusic("forestAmbience.mp3")
        }
        
    }
    
    var isAM = false
    var actualTime = 12
    
    func dayAndNightHandler()
    {
        if actualTime == 12
        {
            isAM = !isAM
        }
        
        timeHud.text = isAM ? "\(actualTime) AM" : "\(actualTime) PM"
        
        self.overlayView.alpha = CGFloat(((cos(time / 57.2957795)) + 1) / 2.5)
        
        if actualTime == 12
        {
            actualTime = 0
        }
        actualTime += 1
        
        // time = x axis
        time += 15
        
        if time == 360
        {
            time = 0
        }

 
        
    }
    
    
    //MENU
    func renderMenu()
    {
        
    }
    
    //TOUCH IMPLEMENTATION
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if newGame == false
        {
            for touch in touches
            {
                
                
                let touchLocation = touch.location(in: cam)
                if actionButton.contains(touchLocation) && inMenu == false
                {
                    if pavils.hero.intersects(houseCollision)
                    {
                        questHandler.gameQuestLabel.removeFromParent()
                        pavils.hero.removeFromParent()
                        if let view = view
                        {
                            let transition:SKTransition = SKTransition.fade(withDuration: 2)
                            let scene: SKScene = House1(size: view.bounds.size)
                            self.view?.presentScene(scene, transition: transition)
                        }
                    }
                }
                
 
                
                if analog.contains(touchLocation) && inMenu == false
                {
                    print("ANALOG PRESSED")
                    pressedControls[analog] = touch as UITouch
                }
                if attackButton.contains(touchLocation) && inMenu == false
                {
                    for tree in trees
                    {
                        if pavils.hero.intersects(tree)
                        {
                            SKTAudio.sharedInstanceEffects().playSoundEffect("woodCrack.wav")
                            let wood = Item(itemIcon: "lumber.png", itemInInventory: false, itemUse: "Fire", itemPositionX: pavils.hero.position.x + 10, itemPositionY: pavils.hero.position.y + 10)
                            pavils.items.append(wood)
                            wood.itemObject.zPosition = 1
                            wood.itemObject.size = CGSize(width: 30, height: 30)
                            self.addChild(wood.itemObject)
                            break
                        }
                    }
                    print("ATTACK PRESSED")
                    inAttack = true
                }
                if sprintButton.contains(touchLocation) && inMenu == false
                {
                    pressedControls[sprintButton] = touch as UITouch
                    sprintButton.texture = sprintPressedTexture
                }
                if inMenu == false && pickUpButton.contains(touchLocation)
                {
                    for i in 0..<pavils.items.count
                    {
                        if pavils.hero.intersects(pavils.items[i].itemObject)
                        {
                            if pavils.numberOfItems < pavils.numberOfItemSlots
                            {
                                let invItem = pavils.items[i]
                                invItem.itemObject.size = CGSize(width: 80, height: 80)
                                invItem.itemObject.zPosition = 14
                                ingameMenu?.items.append(invItem)
                                pavils.numberOfItems += 1
                                pavils.items[i].itemObject.removeFromParent()
                                SKTAudio.sharedInstance().playSoundEffect("swordSwing.mp3")
                                pavils.items.remove(at: i)
                                break
                            }
                            else
                            {
                                let noSpaceMessage = UIAlertController(title: "Pavils has no space for this item!", message: "Throw away some previous items!", preferredStyle: .alert)
                                noSpaceMessage.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.view?.window?.rootViewController?.present(noSpaceMessage, animated: true, completion: nil)
                            }
                        }
                    }
                    
                    for i in 0..<Int((ingameMenu?.items.count)!)
                    {
                        ingameMenu?.items[i].itemObject.position = CGPoint(x: (ingameMenu?.itemSlots[i].position.x)! + (ingameMenu?.itemSlots[i].frame.width)! / 2, y: (ingameMenu?.itemSlots[i].position.y)! + (ingameMenu?.itemSlots[i].frame.height)! / 2)

                        ingameMenu?.items[i].itemObject.isHidden = false
                        if ingameMenu?.items[i].itemObject.parent != cam
                        {
                            cam.addChild((ingameMenu?.items[i].itemObject)!)
                        }
                        
                        ingameMenu?.items[i].itemObject.isHidden = true
                    }
                }
                
                
                if vitalHUD.contains(touchLocation) || vitalityBar.contains(touchLocation) || staminaBar.contains(touchLocation)
                {
                    self.screenshot = captureScreen()
                    SKTAudio.sharedInstance().playSoundEffect("pageTurn.wav")
                    toMenu = true
                    for marker in gameMarkers
                    {
                        marker.isHidden = true
                    }
                    if ingameMenu?.menuButton1Text.fontColor == UIColor.black
                    {
                        ingameMenu?.showMenu1Components()
                    }
                    else if ingameMenu?.menuButton2Text.fontColor == UIColor.black
                    {
                        ingameMenu?.showMenu2Components()
                    }
                    else if ingameMenu?.menuButton3Text.fontColor == UIColor.black
                    {
                        ingameMenu?.showMenu3Components()
                    }
                }
                
                for i in 0..<Int((ingameMenu?.items.count)!)
                {
                    if inMenu == true && ingameMenu?.items[i].itemObject.isHidden == false && (ingameMenu?.items[i].itemObject.contains(touchLocation))!
                    {
                        let droppedItem = ingameMenu?.items[i]
                        ingameMenu?.items[i].itemObject.removeFromParent()
                        droppedItem?.itemObject.zPosition = 1
                        droppedItem?.itemObject.size = CGSize(width: 30, height: 30)
                        droppedItem?.itemObject.position = pavils.hero.position
                        self.addChild((droppedItem?.itemObject)!)
                        ingameMenu?.items.remove(at: i)
                        pavils.numberOfItems -= 1
                        pavils.items.append(droppedItem!)
                        SKTAudio.sharedInstanceEffects().playSoundEffect("swordSwing.mp3")
                        break
                    }
                }
                
                if (ingameMenu?.exitButtonX.contains(touchLocation))! && inMenu == true
                {
                    ingameMenu?.hideAllMenuComponents()
                    ingameMenu?.hidePlayerItems()
                    SKTAudio.sharedInstance().playSoundEffect("pageTurn.wav")
                    inMenu = false
                    for marker in gameMarkers
                    {
                        marker.isHidden = false
                    }
                }
                if inMenu == true && ((ingameMenu?.menuButton1Text.contains(touchLocation))! || (ingameMenu?.menuButton1.contains(touchLocation))!)
                {
                    ingameMenu?.showMenu1Components()
                    ingameMenu?.hideMenu2Subcomponents()
                    ingameMenu?.hidePlayerItems()
                    ingameMenu?.menuButton1Text.fontColor = UIColor.black
                    ingameMenu?.menuButton2Text.fontColor = UIColor.white
                    ingameMenu?.menuButton3Text.fontColor = UIColor.white
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                }
                if inMenu == true && ((ingameMenu?.menuButton2Text.contains(touchLocation))! || (ingameMenu?.menuButton2.contains(touchLocation))!)
                {
                    ingameMenu?.showMenu2Components()
                    ingameMenu?.showPlayerItems()
                    ingameMenu?.menuButton1Text.fontColor = UIColor.white
                    ingameMenu?.menuButton2Text.fontColor = UIColor.black
                    ingameMenu?.menuButton3Text.fontColor = UIColor.white
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                }

                if inMenu == true && ((ingameMenu?.menuButton3Text.contains(touchLocation))! || (ingameMenu?.menuButton3.contains(touchLocation))!)
                {
                    ingameMenu?.hideMenu2Subcomponents()
                    ingameMenu?.showMenu3Components()
                    ingameMenu?.hidePlayerItems()
                    ingameMenu?.menuButton1Text.fontColor = UIColor.white
                    ingameMenu?.menuButton2Text.fontColor = UIColor.white
                    ingameMenu?.menuButton3Text.fontColor = UIColor.black
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                }
                
                if (ingameMenu?.gameMenuSaveButtonText.contains(touchLocation))!
                {
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.black
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    ingameMenu?.showSaveSubmenuComponents()
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                    ingameMenu?.savingGame = true
                    ingameMenu?.loadingGame = false
                }
                if (ingameMenu?.gameMenuLoadButtonText.contains(touchLocation))!
                {
                    ingameMenu?.gameMenuSaveButtonText.fontColor = UIColor.white
                    ingameMenu?.gameMenuLoadButtonText.fontColor = UIColor.black
                    ingameMenu?.gameMenuExitButtonText.fontColor = UIColor.white
                    ingameMenu?.showLoadSubmenuComponents()
                    SKTAudio.sharedInstance().playSoundEffect("click.wav")
                    ingameMenu?.savingGame = false
                    ingameMenu?.loadingGame = true
                }
                if (ingameMenu?.gameMenuExitButtonText.contains(touchLocation))!
                {
                    exit(0)
                }
                
                if inMenu == true && ingameMenu?.savingGame == true
                {
                    if screenshot == nil
                    {
                        screenshot = captureScreen()
                    }
                    
                    let userDefaults = UserDefaults.standard
                    if let data = userDefaults.object(forKey: "savePosition1") as? Data
                    {
                        let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame1")!)
                        ingameMenu?.savePosition1Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition2") as? Data
                    {
                        let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame2")!)
                        ingameMenu?.savePosition2Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition3") as? Data
                    {
                        let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame3")!)
                        ingameMenu?.savePosition3Image.texture = saveGameTexture
                    }

                    
                    if (ingameMenu?.savePosition1.contains(touchLocation))!
                    {
                        SKTAudio.sharedInstance().playSoundEffect("click.wav")
                        let gamesave = Gamesave(worldType: self.worldType, playerPositionX: pavils.hero.position.x, playerPositionY: pavils.hero.position.y)//, saveScreenshot: screenshot!)//, items: pavils.items, inventoryItems: (ingameMenu?.items)!)
                        if let screen = screenshot?.scaleImage(toSize: CGSize(width: (ingameMenu?.savePosition1Image.frame.width)!, height: (ingameMenu?.savePosition1Image.frame.height)!))
                        {
                            screenshot = screen
                            screenshotHandler.saveToFileSystem(name: "saveGame1", image: screenshot!)
                        }
                        
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: gamesave)
                        userDefaults.set(encodedData, forKey: "savePosition1")
                        userDefaults.synchronize()
                        print("SAVED 1")
                        
                        let saveGameTexture = SKTexture(image: screenshot!)
                        ingameMenu?.savePosition1Image.texture = saveGameTexture
                    }
                    if (ingameMenu?.savePosition2.contains(touchLocation))!
                    {
                        SKTAudio.sharedInstance().playSoundEffect("click.wav")
                        let gamesave = Gamesave(worldType: self.worldType, playerPositionX: pavils.hero.position.x, playerPositionY: pavils.hero.position.y)//, saveScreenshot: screenshot!)//, items: pavils.items, inventoryItems: (ingameMenu?.items)!)
                        if let screen = screenshot?.scaleImage(toSize: CGSize(width: (ingameMenu?.savePosition2Image.frame.width)!, height: (ingameMenu?.savePosition2Image.frame.height)!))
                        {
                            screenshot = screen
                            screenshotHandler.saveToFileSystem(name: "saveGame2", image: screenshot!)
                        }
                        
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: gamesave)
                        userDefaults.set(encodedData, forKey: "savePosition2")
                        userDefaults.synchronize()
                        print("SAVED 2")
                        
                        let saveGameTexture = SKTexture(image: screenshot!)
                        ingameMenu?.savePosition2Image.texture = saveGameTexture
                    }
                    if (ingameMenu?.savePosition3.contains(touchLocation))!
                    {
                        SKTAudio.sharedInstance().playSoundEffect("click.wav")
                        let gamesave = Gamesave(worldType: self.worldType, playerPositionX: pavils.hero.position.x, playerPositionY: pavils.hero.position.y)//, saveScreenshot: screenshot!)//, items: pavils.items, inventoryItems: (ingameMenu?.items)!)
                        if let screen = screenshot?.scaleImage(toSize: CGSize(width: (ingameMenu?.savePosition3Image.frame.width)!, height: (ingameMenu?.savePosition3Image.frame.height)!))
                        {
                            screenshot = screen
                            screenshotHandler.saveToFileSystem(name: "saveGame3", image: screenshot!)
                        }
                        
                        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: gamesave)
                        userDefaults.set(encodedData, forKey: "savePosition3")
                        userDefaults.synchronize()
                        print("SAVED 3")
                        
                        let saveGameTexture = SKTexture(image: screenshot!)
                        ingameMenu?.savePosition3Image.texture = saveGameTexture

                    }
                }
                
                if inMenu == true && ingameMenu?.loadingGame == true
                {
                    let userDefaults = UserDefaults.standard
                    if let data = userDefaults.object(forKey: "savePosition1") as? Data
                    {
                        let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame1")!)
                        ingameMenu?.loadPosition1Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition2") as? Data
                    {
                        let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame2")!)
                        ingameMenu?.loadPosition2Image.texture = saveGameTexture
                    }
                    if let data = userDefaults.object(forKey: "savePosition3") as? Data
                    {
                        let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                        let saveGameTexture = SKTexture(image: screenshotHandler.loadImageByName(name: "saveGame3")!)
                        ingameMenu?.loadPosition3Image.texture = saveGameTexture
                    }
                    
                    if (ingameMenu?.loadPosition1.contains(touchLocation))!
                    {
                        if let data = userDefaults.object(forKey: "savePosition1") as? Data
                        {
                            let saveGame1 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                            loadedGame = Gamesave(worldType: self.worldType, playerPositionX: saveGame1.playerPositionX, playerPositionY: saveGame1.playerPositionY)//, saveScreenshot: saveGame1.saveScreenshot!)//, items: saveGame1.items, inventoryItems: saveGame1.inventoryItems)
                            if let view = view
                            {
                                SKTAudio.sharedInstance().playSoundEffect("click.wav")
                                let transition:SKTransition = SKTransition.fade(withDuration: 2)
                                var scene = SKScene()
                                if loadedGame?.worldType == "GameScene"
                                {
                                    scene = GameScene(size: view.bounds.size)
                                }
                                else if loadedGame?.worldType == "House1"
                                {
                                    scene = House1(size: view.bounds.size)
                                }
                                self.view?.presentScene(scene, transition: transition)
                            }
                        }
                    }
                    if (ingameMenu?.loadPosition2.contains(touchLocation))!
                    {
                        if let data = userDefaults.object(forKey: "savePosition2") as? Data
                        {
                            let saveGame2 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                            loadedGame = Gamesave(worldType: self.worldType, playerPositionX: saveGame2.playerPositionX, playerPositionY: saveGame2.playerPositionY)//, saveScreenshot: saveGame2.saveScreenshot!)//, items: saveGame2.items, inventoryItems: saveGame2.inventoryItems)
                            if let view = view
                            {
                                SKTAudio.sharedInstance().playSoundEffect("click.wav")
                                let transition:SKTransition = SKTransition.fade(withDuration: 2)
                                var scene = SKScene()
                                if loadedGame?.worldType == "GameScene"
                                {
                                    scene = GameScene(size: view.bounds.size)
                                }
                                else if loadedGame?.worldType == "House1"
                                {
                                    scene = House1(size: view.bounds.size)
                                }
                                self.view?.presentScene(scene, transition: transition)
                            }
                        }
                    }
                    if (ingameMenu?.loadPosition3.contains(touchLocation))!
                    {
                        if let data = userDefaults.object(forKey: "savePosition3") as? Data
                        {
                            let saveGame3 = NSKeyedUnarchiver.unarchiveObject(with: data) as! Gamesave
                            loadedGame = Gamesave(worldType: self.worldType, playerPositionX: saveGame3.playerPositionX, playerPositionY: saveGame3.playerPositionY)//, saveScreenshot: saveGame3.saveScreenshot!)//, items: saveGame3.items, inventoryItems: saveGame3.inventoryItems)
                            if let view = view
                            {
                                SKTAudio.sharedInstance().playSoundEffect("click.wav")
                                let transition:SKTransition = SKTransition.fade(withDuration: 2)
                                var scene = SKScene()
                                if loadedGame?.worldType == "GameScene"
                                {
                                    scene = GameScene(size: view.bounds.size)
                                }
                                else if loadedGame?.worldType == "House1"
                                {
                                    scene = House1(size: view.bounds.size)
                                }
                                self.view?.presentScene(scene, transition: transition)
                            }
                        }
                    }
                }

            }
        }
        else if newGame == true
        {
            for touch in touches
            {
                let touchLocation = touch.location(in: cam)
                if skipButton.contains(touchLocation)
                {
                    for marker in gameMarkers
                    {
                        marker.isHidden = false
                    }
                    
                    timeHud.isHidden = false
                    nameHUDLabel.isHidden = false
                    vitalHUD.isHidden = false
                    vitalityBar.isHidden = false
                    staminaBar.isHidden = false
                    newGame = false
                    videoPlayer.pause()
                    //videoPlayer.isHidden = true
                    skipButton.isHidden = true
                    videoPlayer.removeFromParent()
                    SKTAudio.sharedInstance().playBackgroundMusic("forestAmbience.mp3")
                }
            }
        }
        
    }
    
    func captureScreen() -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions((self.view?.bounds.size)!, true, 0)
        self.view?.drawHierarchy(in: (self.view?.bounds)!, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        touch = touches.first
        touchLocation = touch!.location(in: cam)
        previousLocation = touch!.previousLocation(in: cam)
        
        if pressedControls[analog] != nil && (analog.intersects(analogDefault) || analog.intersects(analogUp) || analog.intersects(analogDown) || analog.intersects(analogLeft) || analog.intersects(analogRight))
        {
            /*var analogX = analog.position.x + ((touchLocation?.x)! - (previousLocation?.x)!)
            var analogY = analog.position.y + ((touchLocation?.y)! - (previousLocation?.y)!)
            analogX = max(analogX, analog.size.width / 2)
            analogX = min(analogX, size.width - analog.size.width / 2)
            analogY = max(analogY, analog.size.height / 2)
            analogY = min(analogY, size.height - analog.size.height / 2)*/
            
            analog.position = CGPoint(x: (touchLocation?.x)!, y: (touchLocation?.y)!)
            
            /*if !(analogX > self.frame.width / 4 || analogY > self.frame.height / 2.5)
            {
                analog.position = CGPoint(x: analogX, y: analogY)
            }*/
        }
        
        else
        {
            let analogHeight = analog.size.height
            analog.position = CGPoint(x: analogDefault.position.x + analogHeight / 2, y: analogDefault.position.y + analogHeight / 2)
        }
        
    
        
        /*for touch in touches
        {
            let touchLocation = touch.location(in: cam)
            
            if analog.contains(touchLocation)
            {
                return
            }
            if (!analog.contains(touchLocation) && pressedControls[analog] != nil) && !sprintButton.contains(touchLocation)
            {
                pressedControls[analog] = nil
                analog.position = convert(CGPoint(x: self.frame.width / 7, y: self.frame.midY / 2), to: cam)
                pavils.animationIterator = 0
            }
            /*if !sprintButton.contains(touchLocation) && pressedControls[sprintButton] != nil
            {
                pressedControls[sprintButton] = nil
                sprintButton.texture = sprintTexture
            }*/

        }*/

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if pavils.stamina < pavils.maxstamina
        {
            pavils.stamina += 0.05
            staminaBar.size = CGSize(width: pavils.stamina, height: 10)
            staminaBar.zPosition = 8
            //cam.addChild(staminaBar)
        }

        
        pavils.playingSound = false
        for touch in touches
        {
            let touchLocation = touch.location(in: cam)
            if analog.contains(touchLocation) && pressedControls[analog] != nil
            {
                pressedControls[analog] = nil
                let analogHeight = analog.size.height
                analog.position = CGPoint(x: analogDefault.position.x + analogHeight / 2, y: analogDefault.position.y + analogHeight / 2)
                pavils.animationIterator = 0
                pavils.moving = false

            }
            if sprintButton.contains(touchLocation) && pressedControls[sprintButton] != nil
            {
                pressedControls[sprintButton] = nil
                sprintButton.texture = sprintTexture
            }
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact)
    {
        if (contact.bodyA.categoryBitMask == playerCategory) && (contact.bodyB.categoryBitMask == treeCategory)
        {
            print("INTERSECTS")
            pavils.intersects = true
        }
        else
        {
            pavils.intersects = false
        }
    }
    
    //GAMELOOP
    override func update(_ currentTime: TimeInterval)
    {
        if questHandler.activeQuest == 0
        {
            switch questHandler.activeQuestStep
            {
                case -1:
                    if pavils.hero.intersects(questHandler.quest1Marker)
                    {
                        questHandler.quest1Marker.removeFromParent()
                        questHandler.quest1MapMarker.removeFromParent()
                        questHandler.activeQuestStep += 1
                        questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                    }
                case 0:
                    for item in pavils.items
                    {
                        if item.itemUse == "Fire"
                        {
                            questHandler.activeQuestStep += 1
                            questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                        }
                    }
                case 1:
                    var numberOfWoodIterator = 0
                    for item in (ingameMenu?.items)!
                    {
                        if item.itemUse == "Fire"
                        {
                            numberOfWoodIterator += 1
                        }
                    }
                    if numberOfWoodIterator == 3
                    {
                        questHandler.quest1Marker.position = CGPoint(x: -158.569, y: 844.411)
                        questHandler.quest1MapMarker.position = CGPoint(x: (((questHandler.quest1Marker.position.x + 4722) / 39) - 84), y: (((questHandler.quest1Marker.position.y + 283) / 41) - 94.5))
                        self.addChild(questHandler.quest1Marker)
                        gameMap.addChild(questHandler.quest1MapMarker)
                        questHandler.activeQuestStep += 1
                        questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                    }
                case 2:
                    if pavils.hero.intersects(questHandler.quest1Marker)
                    {
                        questHandler.quest1Marker.removeFromParent()
                        questHandler.quest1MapMarker.removeFromParent()
                        questHandler.activeQuestStep += 1
                        questHandler.gameQuestLabel.text = questHandler.quest1Descriptions[questHandler.activeQuestStep]
                    }
                default:
                    break
            }
            
        }
        
        //(width: 168.3, height: 189.3)
        
        
        var hidePickupButton: Bool = true
        for item in pavils.items
        {
            if pavils.hero.intersects(item.itemObject)
            {
                pickUpButton.isHidden = false
                hidePickupButton = false
                break
            }
        }
        
        if hidePickupButton == true
        {
            pickUpButton.isHidden = true
        }
        
        
        millWheel.texture = millTextures[Int(millWheelIterator / 4)]
        millWheelIterator += 1
        if millWheelIterator == 5 * 4 - 1
        {
            millWheelIterator = 0
        }
        
        gameMarker.position = CGPoint(x: (((pavils.hero.position.x + 4722) / 39) - 84), y: (((pavils.hero.position.y + 283) / 41) - 94.5))
        
        var useWaterSound: Bool = false
        
        if pavils.hero.intersects(worldB2) || pavils.hero.intersects(worldC1) || pavils.hero.intersects(worldC2) || pavils.hero.intersects(worldD1)
        {
            useWaterSound = true
        }
        
        if useWaterSound == true
        {
            if SKTAudio.sharedInstanceWater().backgroundMusicPlayer?.isPlaying == false
            {
                SKTAudio.sharedInstanceWater().playBackgroundMusic("river.mp3")
            }
        }
        else
        {
            if SKTAudio.sharedInstanceWater().backgroundMusicPlayer?.isPlaying == true
            {
                SKTAudio.sharedInstanceWater().pauseBackgroundMusic()
            }
        }
        
        for surface in waterSurfaces
        {
            surface.texture = waterTextures[waterSurfaceIterator]
        }
        
        
        
        waterSurfaceIterator += 1
        
        if waterSurfaceIterator == 10
        {
            waterSurfaceIterator = 0
        }
        
        if inAttack == true
        {
            if pavils.direction == "UP"
            {
                pavils.hero.texture = pavils.heroAttackTexturesU[Int(pavils.attackAnimIterator / 4)]
            }
            else if pavils.direction == "DOWN"
            {
                pavils.hero.texture = pavils.heroAttackTexturesS[Int(pavils.attackAnimIterator / 4)]
            }
            else if pavils.direction == "LEFT"
            {
                pavils.hero.texture = pavils.heroAttackTexturesA[Int(pavils.attackAnimIterator / 4)]
            }
            else if pavils.direction == "RIGHT"
            {
                pavils.hero.texture = pavils.heroAttackTexturesD[Int(pavils.attackAnimIterator / 4)]
            }
            
            if Int(pavils.attackAnimIterator / 4) == 3
            {
                SKTAudio.sharedInstanceEffects().playSoundEffect("swordSwing.mp3")
            }
            
            pavils.attackAnimIterator += 1
            
            if pavils.attackAnimIterator == 11 * 4 - 1
            {
                pavils.attackAnimIterator = 0
                inAttack = false
            }
        }
        
        if inMenu == true
        {
            overlayView.isHidden = true
        }
        else
        {
            overlayView.isHidden = false
        }
        if toMenu == true
        {
            for component in (ingameMenu?.menuComponents)!
            {
                component.isHidden = false
            }
            toMenu = false
            inMenu = true
        }
        if inMenu != true
        {
            //CAMERA MOVEMENT
            let playerLocation = CGPoint(x: pavils.hero.frame.midX, y: pavils.hero.frame.midY)
            cam.position = playerLocation
            
            for area in actionAreas
            {
                if area.intersects(pavils.hero)
                {
                    actionButton.isHidden = false
                    break
                }
                actionButton.isHidden = true
            }
            
            //ANALOG CONTROLS IMPLEMENTATION
            if pressedControls[analog] != nil
            {
                pavils.intersects = false
                var movementSpeedConstant: CGFloat = 1.5
                if pressedControls[sprintButton] != nil && pavils.stamina > 5
                {
                    movementSpeedConstant *= 2
                }
                pavils.moving = false
                
                /*for treeCollision in treeCollisions
                 {
                 if pavils.hero.intersects(treeCollision) == true
                 {
                 pavils.intersects = true
                 break
                 }
                 }*/
                
                /*func outOfBounds() -> Bool
                {
                    if pavils.hero.position.x < 1910.65 && pavils.hero.position.x > -4722.29 && pavils.hero.position.y > 283.194 && pavils.hero.position.y < 7506.55
                    {
                        return false
                    }
                    else
                    {
                        return true
                    }
                }
                */
                if pavils.intersects == false && pressedControls[analog] != nil
                {
                    print("PLAYERPOSITION: x:\(pavils.hero.position.x) y:\(pavils.hero.position.y)")
                    //RIGHT
                    if analog.intersects(analogRight) && pavils.hero.position.x < 1910.65
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x + movementSpeedConstant, y: pavils.hero.position.y)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesR[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "RIGHT"
                    }
                    //LEFT
                    if analog.intersects(analogLeft) && pavils.hero.position.x > -4722.29
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x - movementSpeedConstant, y: pavils.hero.position.y)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesW[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "LEFT"
                    }
                    //UP
                    if analog.intersects(analogUp) && pavils.hero.position.y < 7545.93
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x, y: pavils.hero.position.y + movementSpeedConstant)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesU[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "UP"
                    }
                    //DOWN
                    if analog.intersects(analogDown) && pavils.hero.position.y > -283.194
                    {
                        pavils.hero.position = CGPoint(x: pavils.hero.position.x, y: pavils.hero.position.y - movementSpeedConstant)
                        if inAttack == false
                        {
                            pavils.hero.texture = pavils.heroTexturesD[Int(pavils.animationIterator / 4)]
                        }
                        pavils.moving = true
                        pavils.direction = "DOWN"
                    }
                    if pavils.playingSound == false
                    {
                        SKTAudio.sharedInstanceEffects().playBackgroundMusic("walking.mp3")
                        pavils.playingSound = true
                    }
                }
                
                pavils.animationIterator += 1
                if pavils.animationIterator == 11 * 4 - 1
                {
                    pavils.animationIterator = 0
                }
            }
            else
            {
                SKTAudio.sharedInstanceEffects().pauseBackgroundMusic()
            }
            
            //PLAYER VITALITY AND STAMINA BARS UI IMPLEMENTATION
            
            //VITALITY BAR
            
            //STAMINA BAR
            if pavils.moving == true && pressedControls[sprintButton] != nil && pavils.stamina > 0
            {
                pavils.stamina -= 0.25
                staminaBar.size = CGSize(width: pavils.stamina, height: 10)
                //cam.addChild(staminaBar)
            }
            if pavils.stamina < pavils.maxstamina && !(pavils.moving == true && pressedControls[sprintButton] != nil)
            {
                pavils.stamina += 0.05
                staminaBar.size = CGSize(width: pavils.stamina, height: 10)
                //cam.addChild(staminaBar)
            }
            
            //VEGETATION OPACITY
            for tree in trees
            {
                if tree.intersects(pavils.hero)
                {
                    if tree.alpha > 0.65
                    {
                        tree.alpha -= 0.01
                    }
                }
                else
                {
                    if tree.alpha < 1.0
                    {
                        tree.alpha += 0.01
                    }
                }
            }
        }
    }
    
    /*vlavo dole PLAYERPOSITION: x: -4722.29 y: -283.194
    vlavo hore PLAYERPOSITION: x: -4722.29 y: 7506.55
    
    vpravo hore PLAYERPOSITION: x: 1910.65 y: 7506.55
    vpravo dole PLAYERPOSITION: x: 1910.65 y: -283.194*/
    
    
    
}

extension UIImage
{
    func scaleImage(toSize newSize: CGSize) -> UIImage?
    {
        let newRect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height).integral
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        if let context = UIGraphicsGetCurrentContext()
        {
            context.interpolationQuality = .high
            let flipVertical = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: newSize.height)
            context.concatenate(flipVertical)
            context.draw(self.cgImage!, in: newRect)
            let newImage = UIImage(cgImage: context.makeImage()!)
            UIGraphicsEndImageContext()
            return newImage
        }
        return nil
    }
}
