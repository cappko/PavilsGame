//
//  QuestHandler.swift
//  PavilsGame
//
//  Created by Vojtech Florko on 02/05/2017.
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import SpriteKit
import GameplayKit

class QuestHandler
{
    var activeQuest: Int = -1
    var activeQuestStep: Int = -1
    var quests: Array<Dictionary<Int, Int>> = []
    var quest1Descriptions: Dictionary<Int, String> = [:]
    var quest1Requirements: Dictionary<Int, Bool> = [:]
    
    var gameQuestLabel = SKLabelNode(fontNamed: "Georgia")
    
    var quest1Marker = SKShapeNode(circleOfRadius: 20)
    var quest1MapMarker = SKShapeNode(circleOfRadius: 4)
    
    init()
    {
        quest1Marker.fillColor = UIColor.blue
        quest1Marker.zPosition = 3
        quest1Marker.position = CGPoint(x: -2093.83, y: 277.251)
        
        quest1MapMarker.fillColor = UIColor.blue
        quest1MapMarker.zPosition = 8
        
        gameQuestLabel.fontColor = UIColor.yellow
        gameQuestLabel.fontSize = 20
        gameQuestLabel.zPosition = 8
        
        var quest1: Dictionary<Int, Int> = [:]
        quest1[0] = 5
        quest1Descriptions[0] = "QUEST: Cut some wood! (Hit any tree with your sword)"
        quest1Descriptions[1] = "QUEST: Collect three wood bundles!"
        quest1Descriptions[2] = "QUEST: Go home (that's the house in the south of the map!)"
        quest1Descriptions[3] = "QUEST: Enter your house"
        quest1Descriptions[4] = "QUEST: Get some sleep"
        

    }
    
    
}
