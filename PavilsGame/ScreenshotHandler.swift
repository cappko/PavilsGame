//
//  ScreenshotHandler.swift
//  PavilsGame
//
//  Created by Vojtech Florko on 02/05/2017.
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit
import SystemConfiguration

var screenshotHandler = ScreenshotHandler()

class ScreenshotHandler
{
    init () {}
    
    func saveToFileSystem(name: String, image: UIImage)
    {
        //SAVING TO FILESYSTEM AS .png
        var documentsDirectory: String?
        var paths: [AnyObject] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as [AnyObject]
        if paths.count > 0
        {
            documentsDirectory = paths[0] as? String
            let savePath = documentsDirectory! + "/" + name + ".png"
            let pngImageData = UIImagePNGRepresentation(image)
            _ = (try? pngImageData!.write(to: URL(fileURLWithPath: savePath), options: [.atomic])) != nil
        }
        return
    }
    
    //LOADING PICTURES FROM STORAGE BY NAME
    func loadImageByName(name: String) -> UIImage?
    {
        var documentsDirectory: String!
        var paths: [AnyObject]! = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as [AnyObject]!
        if paths.count > 0
        {
            documentsDirectory = paths[0] as! String
            let savePath = documentsDirectory! + "/" + name + ".png"
            let image = UIImage(contentsOfFile: savePath)
            if image == nil
            {
                print("missing image at: \(savePath)")
            }
            else
            {
                return image
            }
        }
        return nil
    }
}
